# Home Teaching Helper

Home Teaching Helper is a program to help LDS leaders work with Home and Visiting Teaching data from MLS. It will convert the data to other more useful forms, such as HTML (great for PDAs), simplified CSV for spreadsheet use and Return and Report files.

This was much more useful when home teaching and visisting teaching assignments couldn't be modified on the web.  It may still be useful for those wanting to be able to play around with assignment changes.



## Screen Shots
* Main Interface

![Home Teaching Helper](README-images/hvthelper_1.2.0.jpg)

* Modifying companionship assignments

![Edit Assignments](README-images/edit_assignments.jpg)

* Exporting the data to differnt formats

![Export Data](README-images/export.jpg)

* Sample Report

![Companionship Report](README-images/report.jpg)

## Getting Started

This project uses uses 'java' and 'ant' to build. It also has an eclipse project file.

You'll need the Java Development Kit (>=1.6)

[Java Download](http://java.com)

You'll also need Apache Ant (>=1.6)

[Apache Ant](http://ant.apache.org)



## Building

```
$> ant dist
```
* The binary build will be in the 'dist' directory.
This should work on Windows, Linux and MacOS.

Note: You can download a pre-compiled binary from sourceforge.net
[SourceForge.net Download](https://sourceforge.net/projects/hvthelper/)

## Running
See the HomeTeachingHelperManual.pdf file for more details.

Java 1.6+ is required. Download the latest version from http://java.com

On Windows type or double click the HVTHelper.jar file in Windows Explorer.

On linux/unix/(Mac OS?) boxes just type:
```
$> ./HVTHelperGUI.sh 
```


On Windows type or double click the .bat file in Windows Explorer:
```
C:\>HVTHelper.bat
```

## Non GUI commands
It's best just to use the GUI, but here's a few command line examples of things you can do.

1. Convert Home Teaching and Visiting Teaching lists to a spreadsheet friendly format.
```
C:\HVTHelper> HVTHelper.bat -idir C:\MLSDATA -odir C:\outputdir -format csv
```

2. Convert Home Teaching and Visiting Teaching lists to a simplified HTML format.
```
C:\HVTHelper> HVTHelper.bat -idir C:\MLSDATA -odir C:\outputdir -format html
```

3. You can then convert the HTML output to plucker format using a command like:
```
C:> plucker-build -M 5 -f HVTeaching-10-10-2008 --home-url C:\outputdir\html\index.html
```
This is a great way to keep the info accessible on your palm pilot.
You could, of course use a graphical distiller as well.

You can use similar programs for Window PC PDAs.

## Sample Data

There is some sample data that can be imported in the 'sample_mls_data' directory.


## Contributing

If you'd like to contribute, let me know. 

## Versioning

See CHANGELOG.txt. 

## Authors

* **Aaron DeMille** - [Bitbucket](https://bitbucket.org/ademille), [Linked-In](https://www.linkedin.com/in/aaron-demille-8252605/)

## License

HVTHelper comes with no warranty of any kind, but will hopefully prove
useful. It is released under the GPLv2 license.

For details see LICENSE.txt

See 3RD_PARTY_LICENSES.txt for additional licenses by reused components.

