Version 1.4.3 9/03/2012 ademille
    1. Added the City to the reports per a user request.
Version 1.4.2 9/16/2011 ademille
	1. Default open/save file location to last directory.
	2. Added a more detailed district report.
	3. Added Open and Save buttons.
	4. Added Ctrl-O (Open) and Ctrl-S (Save) shortcuts.
	5. Upgraded xstream so that HVTHelper will work with Java 7.
Version 1.4.1 1/09/2011 ademille
	1. Export 3rd companionship member for RAR.
Version 1.4.0 9/04/2010 ademille
	1. Added "Unassigned" and "District" reports.
	2. Updated documentation to clarify common problems.
	3. Added E-mail fields for teachers and supervisors.
Version 1.3.3 1/31/2010 ademille
    1. Fixed a bug that prevented teachers from being deleted even when they
    weren't part of a companionship.
Version 1.3.2 12/18/2009 ademille
    1. Added support for new MLS HomeTeaching.csv and VisitingTeaching.csv file.
    2. Made the file parser smarter so that it will work with new and old versions of
    the .csv files.
Version 1.3.1 11/26/2009 ademille
    1. Search for HomeTeaching.csv and VisitingTeaching.csv in case insesitive manner.
    2. Fix Report crash if only Home Teaching or Visiting Teaching file is imported.
Version 1.3.0 11/22/2009 ademille
    1. Added reporting capabilities with Assignments Report.
    2. Better sorting of lists for reports.
Version 1.2.1 8/12/2009 ademille
    1. Added <?xml..> header to saved file to better support non-english
    character sets.
Version 1.2.0 5/7/2009 ademille
    1. Added support for 3 teachers in a companionship.
    2. Added a Log Table.
    3. Added better support for linking teachers to companionships.
    4. Fixed a number of bugs, especially in the HTML ...
Version 1.1.1 3/23/2009 ademille 
	1. Fixed duplicate hot-keys for actions.
	2. Create a user's manual.
Version 1.1.0 3/20/2009 ademille
	1. Added the ability to modify the data.  You can now
	use the GUI to enter and manipulate all the model data.
	The data can also be saved to a file and restored. 
Known Bugs:
	1. Deleting a teacher doesn't remove the teacher from the companionship.
	This results in a model that isn't completely consistent, but doesn't really cause problem.
	2. Teacher assigned to multiple companionships don't show up correctly in the companionship html output.
Version 1.0.1 12/26/2008 ademille 
    1. Removed Address from RAR output.
Version 1.0.0 12/24/2008 ademille 
	1. GUI release. This has the first GUI. 
	2. Searching through nodes.
	3. Export Wizard.
	4. Pretty Icons.
	5. Sample Data added.  
Version 0.5.0 12/2/2008 ademille Initial release to source forge.
