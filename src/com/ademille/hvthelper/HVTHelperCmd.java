/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.ademille.hvthelper.output.CsvOutput;
import com.ademille.hvthelper.output.HTMLOutput;
import com.ademille.hvthelper.output.OutputException;
import com.ademille.hvthelper.output.PrintOutput;
import com.ademille.hvthelper.output.RAROutput;
import com.ademille.hvthelper.parser.ParserException;
import com.ademille.hvthelper.parser.MLSParser;

public class HVTHelperCmd {
	
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	private static CommandLine cmdLine = null;
	
	@SuppressWarnings("static-access")
	private static Options createOptions(){
//	Create Options
		
		Options options = new Options();
		
		Option help = new Option("help","Print this message.");
		
		Option inputDirOption = 
			OptionBuilder.withArgName("directory").hasArg().isRequired()
		.withDescription("Input Directory. The directory containing the files exported from MLS")
		.create("idir");
		
		Option outputDirOption = OptionBuilder.withArgName("directory").hasArg()
		.withDescription("Output Directory. Defaults to current working directory.")
		.create("odir");
		
		Option format = OptionBuilder.withArgName("csv,html,plucker,rar").hasArg().isRequired()
		.withDescription("Output format. The desired output format.")
		.create("format");
		
		options.addOption(help);
		options.addOption(inputDirOption);
		options.addOption(outputDirOption);
		options.addOption(format);
		return options;
		
	}
	
	private static boolean parseArgs(String[] args) {
		/////////////////////////////////////////////////
		// Setup and parse the Command Line Options
		/////////////////////////////////////////////////
		boolean retVal = false;
		Options options = createOptions();
		CommandLineParser cmdParser = new GnuParser();
		try {
			cmdLine = cmdParser.parse(options, args);
			String format = cmdLine.getOptionValue("format").toLowerCase();
			if (format.equals("csv") ||
					format.equals("html")||
					format.equals("plucker") ||
					format.equals("rar")){
				retVal = true;
			} else {
				System.err.println("Invalid Output Format");
				showHelp(options);
			}
			
		}
		catch (ParseException pe){
			System.err.println("Invalid Command Line parameters. " + pe.getMessage());
			showHelp(options);
		}
		return retVal;
	}

	private static void showHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
    System.out.println("HVTHelper by Aaron DeMille (Copyright 2008)");
    System.out.println("Version: "+Version.major + "." + Version.minor + "."+ Version.bugfix + " - " + Version.buildTime);
		formatter.printHelp("HVTHelperCmd", options);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Configure log4j.
		DOMConfigurator.configure("log4jconfig.xml");
		//BasicConfigurator.configure();
		//logger.addAppender(new SwingAppender(null));
		
		if (parseArgs(args)){
			
			String outputdir = cmdLine.getOptionValue("odir");
			String inputdir = cmdLine.getOptionValue("idir");
			
			//Make sure there is a terminating slash on the input and output directories.
			if (!inputdir.endsWith(File.separator)){
				inputdir += File.separator;
			} 
			
			// Default output directory to current directory.
			if (null == outputdir || "".equals(outputdir)){ 
				outputdir = ".";
				} 
				if (!outputdir.endsWith(File.separator)){
					outputdir+= File.separator;
				}
			MLSParser pr = new MLSParser();
			try {
				pr.parseFiles(inputdir);
				
				logger.info("Saving Output.");
				String format = cmdLine.getOptionValue("format").toLowerCase();
				if ("html".equals(format)){
					HTMLOutput htmlOut = new HTMLOutput();
					htmlOut.process(outputdir,pr.getHVTeachingInfo());
				} else if ("csv".equals(format)){
					CsvOutput csvOut = new CsvOutput();
					csvOut.process(outputdir, pr.getHVTeachingInfo());
				} else if ("txt".equals(format)){
					PrintOutput po = new PrintOutput();
					po.process(outputdir, pr.getHVTeachingInfo());
				}
				else if ("rar".equals(format)){
					// Return and Report format.
					RAROutput rar = new RAROutput();
					rar.process(outputdir, pr.getHVTeachingInfo());
				} else {
					logger.error("Sorry, " + format + " isn't currently supported. Use html format and run plucker manually.");
				}
			} catch (ParserException e) {
				logger.error("Unable to parse files:" + e.getMessage());
			} catch (OutputException e){
				logger.error("Error creating output. " + e.getMessage());
			}
		}
	}

}
