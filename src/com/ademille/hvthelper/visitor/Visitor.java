/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.visitor;

import java.util.List;

import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.parser.TeachingAssignmentParser;

public interface Visitor {
	void visit(Quorum quorum);
	void visit(District district);
	void visit(Companionship comp);
	void visit(Family family);
	void visit(TeachingAssignmentParser manager);
	void visitUnassigned(List<Family> family);
	void startVisits();
	void endVisits();

}
