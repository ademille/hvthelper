/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.output;

import com.ademille.hvthelper.model.HVTeachingInfo;

/**
 * MLSOutput defines the interface for an MLS output plugin.
 * This will allow many output plugins to be handled in a generic manner.
 * @author ademille
 *
 */
public interface MLSOutput {
	/**
	 * Gets a description of the output module.
	 * @return A description of the output module.
	 */
	String getDescription();
	
	/**
	 * The name of the output module.
	 * @return The name of the output.
	 */
	String getName();
	
	/**
	 * Process the MLS data converting to the desired output format.
	 * @param outputDir
	 * @param data
	 */
	void process(String outputDir,HVTeachingInfo data) throws OutputException;

}
