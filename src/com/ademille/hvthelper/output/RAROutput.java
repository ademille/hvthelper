/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.output;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.log4j.Logger;

import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.HVTeachingInfo;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.model.Teacher;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class RAROutput implements MLSOutput {
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	public RAROutput (){
	}
	
	private void saveTeaching(String directory, TeachingAssignmentInfo manager) {

		String teacherType = manager.isHometeaching() ? "Home Teacher": "Visiting Teacher";
		String household = manager.isHometeaching() ? "Household": "Sister Being Taught";
		String filePrefix = manager.isHometeaching() ? "HT_": "VT_";
		String visitContact = manager.isHometeaching() ? "Visits" : "Contacts";
		// Create a new file for each district.
		for (Quorum q: manager.getQuorums())
		{
			for (District d : q.getDistricts()) {
				String districtFileName = filePrefix
						+ d.getSupervisor().getName().replaceAll(" ", "_") + ".tab";
				// Kill any commas.
				districtFileName = districtFileName.replaceAll(",", "");
				districtFileName = directory + districtFileName;
				try {
					BufferedWriter out = new BufferedWriter(new FileWriter(
							districtFileName));
					PrintWriter pw = new PrintWriter(out);
					logger.info("Saving district to file: " + districtFileName);
					// Loop through each companionship.
					for (Companionship comp : d.getCompanionships()) {
						pw.println(teacherType + "\tPhone\t" + household + "\tPhone\t"
								+ visitContact + "\t\t\t");
						// Create an array of teachers and the families that are visited.
						// This is necessary so that we can have a teacher and a family on
						// the same line
						// in the output file.
						Teacher[] teachers = new Teacher[] { comp.getTeacher1(),
								comp.getTeacher2(), comp.getTeacher3() };
						List<Family> famList = comp.getFamilies();
						// Loop for whichever is greater, the number of teachers or the
						// number of families.
						for (int i = 0; i < Math.max(teachers.length, famList.size()); i++) {
							if (i < teachers.length) {
								// We are on a line that needs teacher info.
								Teacher teacher = teachers[i];
								String tName = "";
								String tPhone = "";
								if (teacher !=null){
									tName = teacher.getName();
									tPhone = teacher.getPhone();
								}
								pw.print(tName + "\t" + tPhone + "\t");
								if (i > famList.size() - 1) {
									// We have more teachers than families. Since the format is
									// pretty dumb
									// insert some tabs fill out the line with just a teacher, but
									// no family.
									pw.print("\t\t");
								}
							} else {
								// We are on a line with a family, but no teacher.
								pw.print("\t\t");
							}

							if (i < famList.size()) {
								// We are on a line that needs family info.
								Family fam = famList.get(i);
								pw.print(fam.getName() + "\t" + fam.getPhone1() + "\t"
										+ "0% (0 mo.)");
							}
							// For some reason the file format requires 3 extra tabs at the
							// end of each line.
							pw.println("\t\t\t");

						}
						pw.println();
					} // end for-each companion

					// Now print out the unassigned
					pw.println(household + "\tPhone\tStreet Address\t" + visitContact);
					for (Family fam : manager.getUnassigned()) {
						// For some reason the file format requires 4 extra tabs at the end
						// of each unassigned line.
						pw.println(fam.getName() + "\t" + fam.getPhone1()
								+ "\tN/A\t0%(0 mo.)\t\t\t\t");
					}
					pw.close();
				} catch (IOException e) {
					logger.error("Unable to create file:" + districtFileName);
					logger.error(e.getMessage());
				}
			}
		} //end each quorum

	}

	public String getDescription() {
		return "This will create Dictrict files that are suitable for upload to the 'Return and Report' web application.";
	}

	public String getName() {
		return "Return and Report Output";
	}

	public void process(String outputDir, HVTeachingInfo data) {
//	 Create the index file
		if (data.getHtInfo() != null) {
			saveTeaching(outputDir, data.getHtInfo());
		}
		if (data.getVtInfo() != null) {
			saveTeaching(outputDir, data.getVtInfo());
		}
		
	}
	
}
