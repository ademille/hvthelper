/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.output;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.HVTeachingInfo;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.model.Teacher;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class HTMLOutput implements MLSOutput {
	private HVTeachingInfo results;
	private static final String FAMILIES= "families.html";
	private static final String FAMILY = "family.html";
	private static final String COMPS= "companionships.html";
	private static final String TEACHERS = "teachers.html";
	private static final String INDEX= "index.html";
	private static final String UNASSIGNED= "unassigned.html";
	private String group;
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	public HTMLOutput (){
		
	}
	
	
	private boolean createDirs(String htPath, String vtPath){
//	Try to create the output directories.
		File htDir = new File(htPath);
		File vtDir = new File(vtPath);
		if ((htDir.mkdirs() && vtDir.mkdirs()) ||
				 (htDir.exists() && vtDir.exists())){
			logger.info("Successfully created Home Teaching and Visiting Teaching Output Directories.");
			logger.info("Home Teaching Output Directory: " + htPath);
			logger.info("Home Teaching Output Directory: " + vtPath);
			return true;
		} else {
			logger.error("Unable to create Home Teaching and Visting Teaching Output Directories.");
			logger.error("Home Teaching Output Directory: " + htPath);
			logger.error("Home Teaching Output Directory: " + vtPath);
			return false;
		}
	}
	
	public void process(String outputDir, HVTeachingInfo data) {
		this.results = data;
		logger.info("*** HTMLOutput ***");
//	Create the index file
		String rootDir = outputDir + "html/";
		String htPath = rootDir + "HT/";
		String vtPath = rootDir + "VT/";
		String htmlFile = rootDir +  INDEX;
		
		if (createDirs(htPath,vtPath)){
			BufferedWriter out;
			try {
				out = new BufferedWriter(new FileWriter(htmlFile));
				writeIndexFile(out);
				out.close();
			} catch (IOException e) {
				logger.error("Error creating index file.");
			}
		
		
		if (results.getHtInfo()!=null){
			saveTeachingToHtml(htPath, results.getHtInfo());
		} 
		if (results.getVtInfo()!=null){
			saveTeachingToHtml(vtPath, results.getVtInfo());
		}
		}
		
		
	}
	
	private void writeIndexFile(BufferedWriter out)throws IOException{
		out.write("<html>\n");
		out.write("<body>");
		if (results.getHtInfo()!=null){
			out.write("<h1><a href=\"HT/"+INDEX+"\">Home Teaching</a></h1><br/>\n");
		}
		if (results.getVtInfo()!=null){
			out.write("<h1><a href=\"VT/"+INDEX+"\">Visiting Teaching</a></h1><br/>\n");
		}
		out.write("</body>");
		out.write("</html>");
}
	
	private void saveTeachingToHtml(String directory, TeachingAssignmentInfo manager){
		String htmlFile = "Nothing";
		try {
				
			group = manager.isHometeaching()?"Home":"Visiting";
						
			//Create the index file
			htmlFile = directory + INDEX;
			BufferedWriter out= new BufferedWriter(new FileWriter(htmlFile));
			writeTeachingIndexFile(out);
			out.close();

			//Get a list of all families
			List<Family> famList = new LinkedList<Family>();
			for (Quorum q: manager.getQuorums()){
				for (District d: q.getDistricts()){
					for (Companionship c: d.getCompanionships()){
							famList.addAll(c.getFamilies());
					}
				}
			}
			famList.addAll(manager.getUnassigned());
			
			//Sort the family list.
			Collections.sort(famList);
			
			//Create the families file
			htmlFile = directory + FAMILIES;
			out= new BufferedWriter(new FileWriter(htmlFile));
			writeFamiliesFile(out, manager, famList);
			out.close();
			
			//Create a list of teachers in alphabetical order.
			//This will allow searching to find the teaching assignments
			//for someone quickly.
			htmlFile = directory + TEACHERS;
			out= new BufferedWriter(new FileWriter(htmlFile));
			writeTeachersFile(out, manager);
			out.close();
			
			//Create an individual file for each family with 
			//additional details like the address and phone number.
			for (Family f: famList){
				htmlFile = directory + f.getId() + FAMILY;
				out= new BufferedWriter(new FileWriter(htmlFile));
				writeFamilyFile(out,f);
				out.close();
			}
			
			//Create the companionship file
			htmlFile = directory + COMPS;
			out = new BufferedWriter(new FileWriter(htmlFile)); 
			writeCompanionShipFile(out, manager);
			out.close();
			
			//Create the unassigned families file
			htmlFile = directory + UNASSIGNED;
			out = new BufferedWriter(new FileWriter(htmlFile)); 
			writeUnassignedFile(out, manager);
			out.close();
			
			
			System.out.println("Output saved to directory:"+directory);
		} catch (IOException e) {
			System.err.println("Unable to create file:"+ htmlFile);
			System.err.println(e.getMessage());
		}
		
	}
	
	private void writeTeachingIndexFile(BufferedWriter out)throws IOException{
		out.write("<html>\n");
		out.write("<body>");
		out.write("<h1>" + group + " Teaching<h1><br/>\n");
		out.write("<h2><a href=\"" + FAMILIES + "\">Families</a></h2><br/>\n");
		out.write("<h2><a href=\"" + TEACHERS + "\">"+ group + " Teachers</a></h2><br/>\n");
		out.write("<h2><a href=\""+ COMPS + "\">Companionships</a></h2><br/>\n");
		out.write("<h2><a href=\"" + UNASSIGNED + "\">Unassigned</a></h2><br/>\n");
		out.write("</body>");
		out.write("</html>");
}

	private void writeTeachersFile(BufferedWriter out, TeachingAssignmentInfo manager)throws IOException{
		out.write("<html>\n");
		out.write("<body>");
		
		//Create a list of all teachers and sort them.
		List<Teacher> teachers = new ArrayList<Teacher>();
		for (Quorum q: manager.getQuorums()){
			for (District d: q.getDistricts()){
				for (Companionship c: d.getCompanionships()){
					if (c.getTeacher1() != null && !teachers.contains(c.getTeacher1())){
						teachers.add(c.getTeacher1());
					}
					if (c.getTeacher2() != null && !teachers.contains(c.getTeacher2())){
						teachers.add(c.getTeacher2());
					}
					if (c.getTeacher3() != null && !teachers.contains(c.getTeacher3())){
						teachers.add(c.getTeacher3());
					}
				}
			}
		}
		//Now sort the teachers.
		Collections.sort(teachers);
		
		out.write("<table border='1'>\n");
		out.write("<h2>" + group + " Teachers</h2><br/>\n");
		writeTeacherAlphabet(out,teachers);
		for (Teacher t:teachers){
			List<Companionship> comps = t.getCompanionships();
			//Write out a link for each companionship.
			int count = 1;
			for (Companionship c:comps){
				if (c!=null && !t.getName().equals("")){
					String compCount = comps.size() > 1? " - " + String.valueOf(count++):"";
					out.write("<tr><td><b><a name=\"" + t.getId() + "\"</a><a href=\"companionships.html#"+c.getId() +"\">"+ t.getName() +  compCount + "</a></b></td>");
				}
			}
		}
		out.write("</table>\n");
		out.write("</body>");
		out.write("</html>");
}
	
	private void writeFamiliesFile(BufferedWriter out, TeachingAssignmentInfo manager, List<Family> famList)throws IOException{
			out.write("<html>\n");
			out.write("<body>");
			out.write("<table border='1'>\n");
			out.write("<h2>" + group + " Teaching Families</h2><br/>\n");
			writeFamilyAlphabet(out,famList);
			for (Family f:famList){
				out.write("<tr><td><b><a name=\"" + f.getId() + "\"</a><a href=\""+ f.getId()+FAMILY +"\">"+ f.getName() + "</a></b></td>");
				Companionship  c = f.getCompanionship();
				if (null == c){
					//Unassigned family.
					out.write("<td><a href=\"unassigned.html\">Unassigned</a></td></tr>\n");
				} else {
					out.write("<td><a href=\"companionships.html#"+c.getId() +"\">" + c.toString()+"</a></td></tr>\n");
				}
			}
			out.write("</table>\n");
			out.write("</body>");
			out.write("</html>");
	}
	
	/**
	 * Create quick links to the families by the first
	 * letter of their last name.
	 * @param families
	 * @throws IOException
	 */
	private void writeFamilyAlphabet(BufferedWriter out, Collection<Family> families) throws IOException{
		//Map a letter to a family ID.
		SortedMap<String, Integer> letterMap = new TreeMap<String, Integer>();
		
		//This assumes the family collection is sorted.
		for (Family f:families){
			//Grab the first character of the name.
			String firstLetter = f.getName().toLowerCase().substring(0, 1);
			if (letterMap.get(firstLetter)==null){
				//Keep track that we already have linked the letter to the first
				//Family starting with that letter.
				letterMap.put(firstLetter, f.getId());
				//Create the link.
				out.write("<a href=\"#"+ f.getId() +"\">"+ firstLetter + "</a> ");
			}
		}
		out.write("<br/>\n");
	}
	
	/**
	 * Create quick links to the teachers by the first
	 * letter of their last name.
	 * @param families
	 * @throws IOException
	 */
	private void writeTeacherAlphabet(BufferedWriter out, Collection<Teacher> teachers) throws IOException{
		//Map a letter to a family ID.
		SortedMap<String, Integer> letterMap = new TreeMap<String, Integer>();
		
		//This assumes the family collection is sorted.
		for (Teacher t:teachers){
			//Grab the first character of the name.
			if (!t.getName().equals("")){
				String firstLetter = t.getName().toLowerCase().substring(0, 1);
				if (letterMap.get(firstLetter)==null){
					//Keep track that we already have linked the letter to the first
					//Teacher starting with that letter.
					letterMap.put(firstLetter, t.getId());
					//Create the link.
					out.write("<a href=\"#"+ t.getId() +"\">"+ firstLetter + "</a> ");
				}
			}
		}
		out.write("<br/>\n");
	}
	
	private void writeFamilyFile(BufferedWriter out, Family f) throws IOException{
		out.write("<html><body>\n");
		out.write("<table border=\"1\">\n");
		out.write("<tr><td><b>"+ f.getName() +"</b></td></tr>\n");
		out.write("<tr><td><b>"+ f.getPhone1() +"</b></td></tr>\n");
		if (f.getPhone2()!=null && !f.getPhone2().equals("")){
			out.write("<tr><td><b>"+ f.getPhone2() +"</b></td></tr>\n");
		}
		out.write("<tr><td><b>"+ f.getStreet1() +"</b></td></tr>\n");
		out.write("</table>\n");
		out.write("</body></html>\n");
	}
	
	private void writeCompanionShipFile(BufferedWriter out, TeachingAssignmentInfo manager) throws IOException{
			out.write("<html>\n");
			out.write("<body>");
			Collection<Quorum> quorums = manager.getQuorums();
			out.write("<h2>Companionships</h2><br/>\n");
				for (Quorum q : quorums) {
					if (manager.isHometeaching()){
						out.write("<h3>"+ q.getName() + "</h3>\n");
					}
					for (District d : q.getDistricts()) {
						writeDistrict(out, d);
					}
				}
			out.write("</body>");
			out.write("</html>");
	}
	
	private void writeDistrict(BufferedWriter out, District d) throws IOException{
		for (Companionship c: d.getCompanionships()){
			out.write("<br>");
			writeCompanionship(out, c);
		}
	}
	
	private void writeCompanionship(BufferedWriter out, Companionship c) throws IOException{
		String teacherNames = c.joinTeachers(" -- ");
		out.write("<table border=\"1\">\n");
		out.write("<tr><td><a name=\""+c.getId()+"\"><b>"+ teacherNames +"</b></a></td></tr>\n");
		out.write("<tr><td colspan=\"2\">");
		for (Family f:c.getFamilies()){
			out.write("<a href=\""+ f.getId()+FAMILY + "\">"+ f.getName()+"</a><br/>\n");
		}
		out.write("</td></tr></table>");
	}
	
	private void writeUnassignedFile(BufferedWriter out, TeachingAssignmentInfo manager)throws IOException{
		out.write("<h2>Unassigned</h2><br/>\n");
		out.write("<table border=\"1\">\n");
		for (Family f:manager.getUnassigned()){
			out.write("<tr><td><a href=\""+ f.getId()+FAMILY + "\">"+ f.getName()+"</a></td></tr>\n");
		}
		out.write("</table>");
	}


	public String getDescription() {
		return "HTML Output creates a useful linked representation of the Home Teaching and Visiting Teaching assignments" +
		"as well as providing a format that can easily be viewed on a PDA using something like plucker.";
	}


	public String getName() {
		return "HTML Output";
	}

}
