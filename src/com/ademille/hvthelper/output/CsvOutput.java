/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.output;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.HVTeachingInfo;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.model.Teacher;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class CsvOutput implements MLSOutput {
	
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	
	private static String row = "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n";
	
	public CsvOutput(){
		
	}

		
	public String getDescription() {
		return "Save a simplified csv file for Home Teaching and Visiting Teaching. " +
				"This file will eliminate the duplicate information that exists in the" +
				"original MLS export and is much easier to work with in a spreadsheet program."; 
	}

	public String getName() {
		return "Simplified CSV output";
	}
	private void printDistricts(PrintWriter pw, Collection<District> districts){
		for (District district:districts){
			pw.printf(row
					,"",district.getSupervisor().getName(),"","","","","","");
			for (Companionship comp: district.getCompanionships()){
				Teacher teacher1 = comp.getTeacher1();
				Teacher teacher2 = comp.getTeacher2();
				Teacher teacher3 = comp.getTeacher3();
				String t1Info, t2Info, t3Info;
				t1Info = (teacher1==null?"":teacher1.getName() + " (" + teacher1.getPhone() + ")");
				t2Info = (teacher2==null?"":teacher2.getName() + " (" + teacher2.getPhone() + ")");
				t3Info = (teacher3==null?"":teacher3.getName() + " (" + teacher3.getPhone() + ")");

				//Make a copy of the family so that it can be sorted 
				//without modifying the underlying model.
				List<Family> famList = new LinkedList<Family>(comp.getFamilies());
				Collections.sort(famList);
				pw.printf(row
						,"","",t1Info,t2Info,t3Info,"","","");
				for (Family family: famList){
					pw.printf(row
							,"","","","","",family.getName(), family.getStreet1(), family.getPhone1());
				}
			}
		}
	}
	
	private void printTeacherAssignments(PrintWriter pw, TeachingAssignmentInfo ta){
		for (Quorum quorum : ta.getQuorums()) {
			String qName = ta.isHometeaching()?quorum.getName():"";
			pw.printf(row
					, qName, "", "", "", "","","","");
			printDistricts(pw, quorum.getDistricts());
		} 
	
		//Print unassigned list.
		pw.printf(row,"","","","","","","","");
		pw.printf(row,"","","","","","","","");
		pw.printf(row, "**** Unassigned ****","","","","","","","");
	
		//Make a copy of the family so that it can be sorted 
		//without modifying the underlying model.
		List<Family> famList = new LinkedList<Family>(ta.getUnassigned());
		Collections.sort(famList);
		for (Family f: famList){
				pw.printf(row
						,"","","","","",f.getName(),f.getStreet1(),f.getPhone1());
		}
		
		
	}
	private PrintWriter getPrintWriter(String fileName){
		PrintWriter pw = null;
		try {
			FileOutputStream out = new FileOutputStream(fileName);
			pw = new PrintWriter(out);
		}
		catch (Exception e){
			logger.error("Unable to open file:"+fileName);
		}
		return pw;
	}
	
	public void process(String outputDir, HVTeachingInfo data) throws OutputException {
		logger.info("*** CsvOutput ***");
		if (data.getHtInfo() == null && data.getVtInfo() == null){
			throw new OutputException("CSV Output requires a HomeTeaching.csv or VisitingTeaching.csv file as input.");
		}
		if (data.getHtInfo()!=null){
			String outputFile = outputDir + "/HomeTeachingAssignments.csv";
			PrintWriter pw = getPrintWriter(outputFile);
			if (pw!=null){
				pw.printf(row, "Quorum", "Supervisor","Home Teacher 1","Home Teacher 2","Home Teacher 3","Household","Address","Phone");
				printTeacherAssignments(pw, data.getHtInfo());
				pw.close();
				logger.info("Saved Home Teaching CSV Output to file: " + outputFile );
			}
			
		}
		if (data.getVtInfo()!=null){
			String outputFile= outputDir + "/VisitingTeachingAssignments.csv";
			PrintWriter pw = getPrintWriter(outputFile);
			if (pw!=null){
				pw.printf(row, "","Supervisor","Visiting Teacher 1","Visiting Teacher 2","Visiting Teacher 3","Household","Address","Phone");
				printTeacherAssignments(pw, data.getVtInfo());
				pw.close();
				logger.info("Saved Visiting Teaching CSV Output to file: " + outputFile );
			}
		}
		
	}

}
