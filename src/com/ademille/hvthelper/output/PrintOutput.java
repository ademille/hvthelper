/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.output;

import java.util.Collection;
import java.util.List;

import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.HVTeachingInfo;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class PrintOutput implements MLSOutput{

	public void visitUnassigned(List<Family> families) {
		
	}

	public String getDescription() {
		return "Displays the teaching assignments as a tree.";
	}

	public String getName() {
		return "Text Output";
	}

	private void printDistricts(Collection<District> districts){
		for (District d:districts){
			System.out.println("\t" + d.getSupervisor().getName());
			for (Companionship comp: d.getCompanionships()){
				String teachersNames = comp.joinTeachers(", ");
				System.out.println("\t\t"+ teachersNames);
				for (Family family: comp.getFamilies()){
					System.out.println("\t\t\t" + family.getName());					
				}
			}
		}
	}
	
	private void printTeacherAssignments(TeachingAssignmentInfo ta){
			for (Quorum q: ta.getQuorums()){
				System.out.println(q.getName());
				printDistricts(q.getDistricts());
			}

		//Print unassigned list.
		for (Family f: ta.getUnassigned()){
			System.out.println("Unassigned:" + f.getName());
		}
		
	}
	
	public void process(String outputDir, HVTeachingInfo data) {
		if (data.getHtInfo()!=null){
			System.out.println("**** Home Teaching Report ****");
			printTeacherAssignments(data.getHtInfo());
		}
		if (data.getVtInfo()!=null){
			System.out.println("**** Visiting Teaching Report ****");
			printTeacherAssignments(data.getHtInfo());
		}
		
	}

}
