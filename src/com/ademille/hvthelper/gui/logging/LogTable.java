/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.logging;

import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Appender;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

import com.ademille.hvthelper.gui.util.ImageUtil;

public class LogTable extends JTable {
	public enum Columns {TYPE, TIME, MESSAGE};
	
	private class Model extends AbstractTableModel {

		private ArrayList<LoggingEvent> logList;
		private SimpleDateFormat formatter;
		private Date date;

		public Model() {
			logList = new ArrayList<LoggingEvent>();
			formatter = new SimpleDateFormat("h:mm a MM/dd/yy");
			formatter.setTimeZone(TimeZone.getDefault());
			date = new Date();
			
		}
		
		public void addLoggingEvent(LoggingEvent logEvent){
			logList.add(logEvent);
			fireTableRowsInserted(logList.size()-1, logList.size()-1);
			//Make sure the new entry is visible.
			scrollRectToVisible(getCellRect(getRowCount()-1, 0, true));
		}
		

		@Override
		public int getColumnCount() {
			// Type, Time,  Message
			return Columns.values().length;
		}
		
		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			if (columnIndex == Columns.TIME.ordinal() || columnIndex == Columns.MESSAGE.ordinal()){
				return true;
			} else {
				return super.isCellEditable(rowIndex, columnIndex);
			}
		}
		
		@Override
		public String getColumnName(int column) {
			String ret = "";
			switch (column){
			case 0:
				ret = "Type";
				break;
			case 1:
				ret = "Time";
				break;
			case 2:
				ret = "Message";
				break;
			default:
					ret = "";
			}
			return ret;
		}

		@Override
		public int getRowCount() {
			return logList.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			Object retVal = null;
			if (rowIndex < logList.size()) {
				LoggingEvent le = logList.get(rowIndex);
				if (columnIndex == Columns.TYPE.ordinal()){
					retVal =  le.getLevel();
				} else if (columnIndex == Columns.TIME.ordinal()){
					date.setTime(le.getTimeStamp());
					retVal = formatter.format(date);
				} else if (columnIndex == Columns.MESSAGE.ordinal()){
					retVal = le.getMessage().toString();
				}
			}
			return retVal;
		}
	}
	
	private class LogAppender extends AppenderSkeleton{

		private Model model = null;
		public LogAppender(Model model){
			this.model = model;
		}
		
		@Override
		protected void append(LoggingEvent logEvent) {
			model.addLoggingEvent(logEvent);
			
		}
		@Override
		public void close() {
			//do nothing
		}

		@Override
		public boolean requiresLayout() {
			return false;
		}
		
	}
	
	private class ImageRenderer extends JLabel implements TableCellRenderer {

		ImageIcon info;
		ImageIcon warning;
		ImageIcon error;
		public ImageRenderer(){
			setOpaque(true); //MUST do this for background to show up.
			info = ImageUtil.createIcon("information.png");
			warning = ImageUtil.createIcon("warning.png");
			error = ImageUtil.createIcon("error.png");
		}
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			Level level = (Level)value;
			setHorizontalAlignment(SwingConstants.CENTER);
			if (level == Level.INFO){
				setIcon(info);	
			} else if (level == Level.WARN){
				setIcon(warning);
			} else {
				setIcon(error);
			}
			return this;
		}
		
	}
	
	
	private LogAppender logAppender;
	private Model model;
	private ImageRenderer renderer;
	public LogTable() {
		super();
		//setSize(400, 100);
		model = new Model();
		logAppender = new LogAppender(model);
		renderer = new ImageRenderer();
		
		setModel(model);
		setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		getColumnModel().getColumn(Columns.TYPE.ordinal()).setPreferredWidth(40);
		getColumnModel().getColumn(Columns.TYPE.ordinal()).setMaxWidth(40);
		getColumnModel().getColumn(Columns.TIME.ordinal()).setPreferredWidth(80);
		getColumnModel().getColumn(Columns.MESSAGE.ordinal()).setPreferredWidth(300);
	}
	
	public Appender getAppender(){
		return logAppender;
	}
	
	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {
		if (column == Columns.TYPE.ordinal()){
			return renderer;
		} else {
			return super.getCellRenderer(row, column);
		}
	}
	

}
