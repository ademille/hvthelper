/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.logging;

import javax.swing.JTextArea;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

public class SwingAppender extends AppenderSkeleton {
	
	private JTextArea textArea;

	public SwingAppender(JTextArea textArea){
		this.textArea = textArea;
	}
	
	@Override
	protected void append(LoggingEvent event) {
		textArea.append("["+ event.getLevel().toString() + "] - ");
		textArea.append(event.getMessage().toString()+"\n");
		textArea.setCaretPosition(textArea.getText().length()-1);
	}

	public void close() {
		//nothing to do.
		
	}

	public boolean requiresLayout() {
		return false;
	}

	
}
