/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ademille.hvthelper.gui.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import com.ademille.hvthelper.gui.GroupType;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.HVTeachingInfo;
import com.ademille.hvthelper.model.Quorum;


public class DistrictReport implements ReportGenerator {
	private GroupType groupType;
	
	public DistrictReport(GroupType groupType){
		this.groupType = groupType;
	}
	
	@Override
	public void showReport(HVTeachingInfo info, CursorIconControl cc) throws JRException {

		//Change the cursor to a waiting state.
		cc.waitCursor();
		
    JasperPrint jasperPrint;
      //Loading my jasper file
      ClassLoader cl = DistrictReport.class.getClassLoader();
      JasperReport jasperReport = (JasperReport) net.sf.jasperreports.engine.util.JRLoader.loadObject(cl
      .getResourceAsStream(reportPath + "DistrictReport.jasper"));

      
      List<District> distList = new ArrayList<District>();
		if (info.getHtInfo() != null) {
			List<Quorum> quorums = info.getHtInfo().getQuorums();
			for (Quorum q : quorums) {
				if ((q.getName().toLowerCase().contains("elder") && groupType == GroupType.ELDERS)
						|| (q.getName().toLowerCase().contains("high") && groupType == GroupType.HIGH_PRIESTS))
					for (District d : q.getDistricts()) {
						distList.add(d);
					}
			}
		}

      if (groupType == GroupType.RELIEF_SOCIETY){
      	if (info.getVtInfo() != null){
	      	List<Quorum> quorums = info.getVtInfo().getQuorums();
	      	for (Quorum q: quorums){
	        	for (District d:q.getDistricts()){
	        		distList.add(d);
	        	}
	        }
	      }
      }
      HashMap<String, String> params = new HashMap<String, String>();
      switch (groupType){
      case ELDERS:
    	  params.put("title", "Elders Quorum Districts");
    	  break;
      case HIGH_PRIESTS:
    	  params.put("title", "High Priests Quorum Districts");
    	  break;
      case RELIEF_SOCIETY:
    	  params.put("title", "Visiting Teaching Districts");
    	  break;
      default:
    	  params.put("title", "Teaching Districts");
      }
      
      jasperPrint = JasperFillManager.fillReport(
          jasperReport, params, new JRBeanCollectionDataSource(distList));
      JasperViewer.viewReport(jasperPrint,false);
      //Change the cursor to a normal state.
  		cc.normalCursor();


	}

}
