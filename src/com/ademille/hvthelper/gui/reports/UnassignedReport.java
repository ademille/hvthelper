/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ademille.hvthelper.gui.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.HVTeachingInfo;

public class UnassignedReport implements ReportGenerator {
	private boolean homeTeaching;
	
	
	public UnassignedReport(boolean homeTeaching){
		this.homeTeaching = homeTeaching;
	}
	
	@Override
	public void showReport(HVTeachingInfo info, CursorIconControl cc) throws JRException {

		//Change the cursor to a waiting state.
		cc.waitCursor();
		
    JasperPrint jasperPrint;
      //Loading my jasper file
      ClassLoader cl = UnassignedReport.class.getClassLoader();
      JasperReport jasperReport = (JasperReport) net.sf.jasperreports.engine.util.JRLoader.loadObject(cl
      .getResourceAsStream(reportPath + "Unassigned.jasper"));

      
      List<Family> unassignedList= new ArrayList<Family>();
      if (homeTeaching && info.getHtInfo()!=null){
        unassignedList= info.getHtInfo().getUnassigned();
      }  else if (info.getVtInfo() != null){
        unassignedList = info.getVtInfo().getUnassigned();
     	}
      
      HashMap<String, String> params = new HashMap<String, String>();
      if (homeTeaching)
        params.put("title", "Unassigned Home Teaching Families");
      else
        params.put("title", "Unassigned Visiting Teaching Families");
        
      jasperPrint = JasperFillManager.fillReport(
          jasperReport, params, new JRBeanCollectionDataSource(unassignedList));
      JasperViewer.viewReport(jasperPrint,false);
      //Change the cursor to a normal state.
  		cc.normalCursor();


	}

}
