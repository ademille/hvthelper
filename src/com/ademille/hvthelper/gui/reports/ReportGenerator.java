/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ademille.hvthelper.gui.reports;

import net.sf.jasperreports.engine.JRException;

import com.ademille.hvthelper.model.HVTeachingInfo;

public interface ReportGenerator {
	
	/**
	 * Show the report based on the information in the HVTeachingInfo object.
	 * @param info The Home and Visiting Teaching Info.
	 * @param cursorControl Provides methods to change the cursor to a waiting cursor
	 * and back to normal.  Can be used by reports during processing time.
	 */
	void showReport(HVTeachingInfo info, CursorIconControl cursorControl) throws JRException;
	
	static final String reportPath = "com/ademille/hvthelper/gui/reports/"; 

}
