package com.ademille.hvthelper.gui.wizards.exporthtml;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardPage;

import com.ademille.hvthelper.gui.util.CenteringFileChooser;

import java.awt.Dimension;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;

public class OutputDirPage extends WizardPage {

	private JTextField txtExportDir = null;
	private JButton btnBrowse = null;
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");  //  @jve:decl-index=0:
	private static File lastDir = null;

	/**
	 * This method initializes 
	 * 
	 */
	public OutputDirPage() {
		super();
		initialize();
	}
	
	public static String getDescription(){
		return "Select Output Directory";
	}

	/**
	 * This method initializes this
	 * 
	 */
	private void initialize() {
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.insets = new Insets(5, 5, 5, 5);
        gridBagConstraints1.gridy = 0;
        gridBagConstraints1.gridx = 1;
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.setLayout(new GridBagLayout());
        this.setSize(new Dimension(460, 89));
        this.add(getTxtExportDir(), gridBagConstraints);
        this.add(getBtnBrowse(), gridBagConstraints1);
			
	}

	/**
	 * This method initializes txtExportDir	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtExportDir() {
		if (txtExportDir == null) {
			txtExportDir = new JTextField();
			txtExportDir.setPreferredSize(new Dimension(300, 25));
			txtExportDir.setName("exportDir");
			if (null != lastDir){
				try {
					txtExportDir.setText(lastDir.getCanonicalPath());
				} catch (IOException e) {
					//Just ignore.
				}
			}
		}
		return txtExportDir;
	}

	/**
	 * This method initializes btnBrowse	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnBrowse() {
		if (btnBrowse == null) {
			btnBrowse = new JButton();
			btnBrowse.setText("Browse");
			btnBrowse.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					
					final JFileChooser fc = new CenteringFileChooser();
					fc.setDialogTitle("Select Output Directory");
					fc.setSelectedFile(lastDir);
					fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					int retVal = fc.showOpenDialog(OutputDirPage.this);
					if (JOptionPane.OK_OPTION == retVal){
						try {
							String dir = fc.getSelectedFile().getCanonicalPath();
							logger.info("Selected Output directory:"+ dir );
							txtExportDir.setText(dir);
						} catch (IOException ioe) {
							logger.error(ioe.getMessage());
						}
					}
				}
			});
		}
		return btnBrowse;
	}
	
	@Override
	protected String validateContents(Component component, Object event) {
		if (component == txtExportDir){
				File path = new File(txtExportDir.getText());
				if (path.isDirectory()){
					lastDir = path;
					return null;
				} else {
					return "Please select an existing diretory.";
				}
		}
		//Everything is fine.
		return null;
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"
