package com.ademille.hvthelper.gui.wizards.exporthtml;

import java.awt.GridBagLayout;

import org.netbeans.spi.wizard.WizardPage;

import javax.swing.ButtonGroup;
import java.awt.GridBagConstraints;
import javax.swing.JRadioButton;

//TODO: Make the population of this page dynamic.  Polymorphism please.

public class FormatPage extends WizardPage {

	private static final long serialVersionUID = 1L;
	private JRadioButton radioCsv = null;
	private JRadioButton radioHtml = null;
	private JRadioButton radioPlucker = null;
	private JRadioButton radioRar = null;
	private ButtonGroup btnGroup = null;

	/**
	 * This is the default constructor
	 */
	public FormatPage() {
		super();
		initialize();
		createRadioGroup();
	}
	
	private void createRadioGroup() {
		btnGroup = new ButtonGroup();
		btnGroup.add(radioCsv);
		btnGroup.add(radioHtml);
		btnGroup.add(radioPlucker);
		btnGroup.add(radioRar);
		
	}

	public static String getDescription(){
		return "Select Output Format";
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
		gridBagConstraints4.gridx = 0;
		gridBagConstraints4.anchor = GridBagConstraints.WEST;
		gridBagConstraints4.gridy = 3;
		GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
		gridBagConstraints3.gridx = 0;
		gridBagConstraints3.anchor = GridBagConstraints.WEST;
		gridBagConstraints3.gridy = 2;
		GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
		gridBagConstraints2.gridx = 0;
		gridBagConstraints2.anchor = GridBagConstraints.WEST;
		gridBagConstraints2.gridy = 1;
		GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
		gridBagConstraints11.gridx = 0;
		gridBagConstraints11.anchor = GridBagConstraints.WEST;
		gridBagConstraints11.gridy = 0;
		this.setSize(435, 145);
		this.setLayout(new GridBagLayout());
		this.add(getRadioCsv(), gridBagConstraints11);
		this.add(getRadioHtml(), gridBagConstraints2);
		this.add(getRadioPlucker(), gridBagConstraints3);
		this.add(getRadioRar(), gridBagConstraints4);
		
	}

	/**
	 * This method initializes radioCsv	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRadioCsv() {
		if (radioCsv == null) {
			radioCsv = new JRadioButton();
			radioCsv.setText("Comma Separated Value (CSV)");
			radioCsv.setSelected(true);
			radioCsv.setToolTipText("Simplified CSV output that imports easily into spreadsheets.");
			radioCsv.setName("radioCsv");
		}
		return radioCsv;
	}

	/**
	 * This method initializes radioHtml	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRadioHtml() {
		if (radioHtml == null) {
			radioHtml = new JRadioButton();
			radioHtml.setText("HTML (Great for converting to PDA formats)");
			radioHtml.setToolTipText("You can use html to e-book converters to put this information on your PDA.");
			radioHtml.setName("radioHtml");
		}
		return radioHtml;
	}

	/**
	 * This method initializes radioPlucker	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRadioPlucker() {
		if (radioPlucker == null) {
			radioPlucker = new JRadioButton();
			radioPlucker.setText("Plucker (Palm E-book Reader format)");
			radioPlucker.setToolTipText("Plucker is a great reader program for palm.");
			radioPlucker.setName("radioPlucker");
		}
		return radioPlucker;
	}

	/**
	 * This method initializes radioRar	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRadioRar() {
		if (radioRar == null) {
			radioRar = new JRadioButton();
			radioRar.setText("Return and Report");
			radioRar.setToolTipText("Create district files that can be imported into Return and Report.");
			radioRar.setName("radioRar");
		}
		return radioRar;
	}

	
}  //  @jve:decl-index=0:visual-constraint="10,10"
