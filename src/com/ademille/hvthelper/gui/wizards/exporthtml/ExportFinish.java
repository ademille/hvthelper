package com.ademille.hvthelper.gui.wizards.exporthtml;

import java.util.Map;

import javax.swing.JLabel;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.Summary;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.ademille.hvthelper.model.HVTeachingInfo;
import com.ademille.hvthelper.output.CsvOutput;
import com.ademille.hvthelper.output.HTMLOutput;
import com.ademille.hvthelper.output.MLSOutput;
import com.ademille.hvthelper.output.OutputException;
import com.ademille.hvthelper.output.RAROutput;

public class ExportFinish implements WizardResultProducer {

	private HVTeachingInfo info;
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");  //  @jve:decl-index=0:
	
	public ExportFinish(HVTeachingInfo info) {
		this.info = info;
	}
	@SuppressWarnings("rawtypes")
	@Override
	public boolean cancel(Map settings) {
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object finish(Map wizardData) throws WizardException {
		String result = "<html>";
		String exportDir = (String)wizardData.get("exportDir");
		String separator = System.getProperty("file.separator");
		if (!exportDir.endsWith("/") && !exportDir.endsWith("\\")){
			exportDir += separator;
		}
		//TODO: Figure out a better way to do this.  Maybe some polymorphism?
		
		//Choose the correct exporter.
		MLSOutput output = null;
		result += "Output Format: ";
		if ((Boolean)wizardData.get("radioCsv")){
			result += "<b>CSV</b>";
			output = new CsvOutput();
		} 
		else if ((Boolean)wizardData.get("radioHtml")){
			result += "<b>HTML</b>";
			output = new HTMLOutput();
		}
		else if ((Boolean)wizardData.get("radioPlucker")){
			result += "<b>Plucker</b>";
			result += "<br><p>Plucker isn't directly supported at this time so the data has been exported as HTML." +
					"  You can easily run the distiller on the html output." +
					"<br/>To convert to plucker format, run the following command or use " +
					"Sunrise.<br>" +
					"<font color=blue><em> $>plucker-build -M 5 -f HVTeaching --home-url " + exportDir + "html" + separator + "index.html</em></font></p>";
			output = new HTMLOutput();
		}
		
		else if ((Boolean)wizardData.get("radioRar")){
			result += "<b>Return and Report</b>";
			output = new RAROutput();
		} else {
			result += "<b>Unknown. How did that happen?</b>";
			//This should never happen.
			logger.error("Unable to find valid exporter.  This should never happen!");
		}
		
		result += "<br>Output Directory: <b>" + exportDir + "</b>";
		//Finally export the data.
		if (null != output) {
			try {
				output.process(exportDir, info);
			} catch (OutputException e) {
				result += "<br><font color=red>Error exporting data: " + e.getMessage() + "</font>";
				logger.error(e.getMessage());
			}
		}
		result +="</html>";
		JLabel lblResult = new JLabel(result);
		return  Summary.create(lblResult, null);
	}

}
