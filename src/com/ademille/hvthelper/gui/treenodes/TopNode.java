/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import javax.swing.ImageIcon;

import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.HVTeachingInfo;

public class TopNode extends HVTHelperNode{
	private HVTeachingInfo info;
	
	public TopNode(HVTeachingInfo info) {
		this.info = info;
	}
	
	@Override
	public String toString() {
		return "Organizations";
	}

	@Override
	public void populate() {
		if (null != info){
		if (null != info.getHtInfo()) {
				addHVTHelperNode(new AssignmentsNode(info.getHtInfo()));
			}

			if (null != info.getVtInfo()) {
				addHVTHelperNode(new AssignmentsNode(info.getVtInfo()));
			}
		}
	}
	
	@Override
	public ImageIcon getIcon() {
		return ImageUtil.createIcon("toc_closed.gif");
	}

}
