/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import javax.swing.ImageIcon;

import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class AssignmentsNode extends HVTHelperNode{
	private TeachingAssignmentInfo info;
	public AssignmentsNode(TeachingAssignmentInfo info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return info.isHometeaching()? "Home Teaching":"Visiting Teaching";
	}

	@Override
	public void populate() {
		
		HVTHelperNode unassignedNode = new UnassignedNode(info.getUnassigned());
		
		for (Quorum q: info.getQuorums()){
			addHVTHelperNode(new QuorumNode(q, info, unassignedNode));
		}
	
		//Add the teachers
		addHVTHelperNode(new TeacherListNode(info));
		//Add the unassigned.
		addHVTHelperNode(unassignedNode);
	}
	
	@Override
	public ImageIcon getIcon() {
		if (info.isHometeaching()){
			return ImageUtil.createIcon("user_suit.png");
		} else {
			return  ImageUtil.createIcon("user_female.png");
		}
	}

}
