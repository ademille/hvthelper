/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.ImageIcon;

import com.ademille.hvthelper.gui.dialogs.EditDistrictDialog;
import com.ademille.hvthelper.gui.util.HVTAction;
import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class QuorumNode extends HVTHelperNode {

	private Quorum quorum;
	private TeachingAssignmentInfo info;
	private HVTHelperNode unassignedNode;
	
	
	public QuorumNode(Quorum quorum, TeachingAssignmentInfo info, HVTHelperNode unassignedNode) {
		this.quorum = quorum;
		this.info = info;
		this.unassignedNode = unassignedNode;
		createActions();
	}
	
	private void createActions(){
		Action a = new HVTAction("Add District",
				ImageUtil.createIcon("telephone_add.png"),
				"Add a New District",
				new Integer(KeyEvent.VK_A),
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditDistrictDialog dlg = new EditDistrictDialog(getTopFrame(), null);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							District d = dlg.getDistrict();
							quorum.addDistrict(d);
							//Figure out where the item was added, so we can add a GUI node at the same
							//place. This will make sure the tree reflects the actual data structure.
							int pos = quorum.getDistricts().indexOf(d);
							addHVTHelperNode(new DistrictNode(quorum.getDistricts(), d, info, unassignedNode), pos);
						}
					}
				});
		actionList.add(a);	
	}
	
	@Override
	public void populate() {
		for (District d: quorum.getDistricts()){
			addHVTHelperNode(new DistrictNode(quorum.getDistricts(), d, info, unassignedNode));
		}
	}
	
	@Override
	public String toString() {
		return quorum.getName();
	}
	
	@Override
	public ImageIcon getIcon() {
		return ImageUtil.createIcon("group.gif");
	}

}
