/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;

import com.ademille.hvthelper.gui.dialogs.EditFamilyDialog;
import com.ademille.hvthelper.gui.util.HVTAction;
import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.Family;

public class UnassignedNode extends HVTHelperNode {
	
	private List<Family> families;
	public UnassignedNode(List<Family> families) {
		this.families = families;
		createActions();
	}
	
	private void createActions(){
		Action a = new HVTAction("Add Family",
				ImageUtil.createIcon("house_add.png"),
				"Add Family",
				KeyEvent.VK_E,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditFamilyDialog dlg = new EditFamilyDialog(getTopFrame(), null);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							Family f = dlg.getFamily();
							families.add(f);
							// Figure out where the item was added, so we can add a GUI node
							// at the same place. This will make sure the tree reflects the actual data
							// structure.
							int pos = families.indexOf(f);
							addHVTHelperNode(new FamilyNode(f,families, false),pos);
						}
					}
				});
		actionList.add(a);	
	}

	@Override
	public void populate() {
		for (Family f: families){
			addHVTHelperNode(new FamilyNode(f,families,false));
		}
	}
	
	@Override
	public String toString() {
		return "Unassigned";
	}
	
	@Override
	public String[][] getTableData() {
		return new String [][] {
				{ "Number Unassigned", "" + families.size()}
		};
	}
	
	@Override
	public ImageIcon getIcon() {
		return ImageUtil.createIcon("house-question.png");
	}

}
