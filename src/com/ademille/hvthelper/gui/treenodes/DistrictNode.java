/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.ademille.hvthelper.gui.dialogs.EditCompanionshipDialog;
import com.ademille.hvthelper.gui.dialogs.EditDistrictDialog;
import com.ademille.hvthelper.gui.util.HVTAction;
import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class DistrictNode extends HVTHelperNode {

	private District district;
	private Collection<District> districts;
	private TeachingAssignmentInfo info;
	private HVTHelperNode unassignedNode;
	
	public DistrictNode(Collection<District> districts, District district, TeachingAssignmentInfo info, HVTHelperNode unassignedNode) {
		this.districts = districts;
		this.district= district;
		this.info = info;
		this.unassignedNode = unassignedNode;
		createActions();
		
	}
	
	private void createActions() {
		Action a = new HVTAction("Add Companionship",
				ImageUtil.createIcon("group_add.png"),
				"Add a New Companionship",
				KeyEvent.VK_C,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditCompanionshipDialog dlg = new EditCompanionshipDialog(getTopFrame(), null, info);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							Companionship c  = dlg.getCompanionship();
							district.addCompanionship(c);
							addHVTHelperNode(new CompanionshipNode(c,district, info,unassignedNode));
						}
					}
				});
		actionList.add(a);
		
		a = new HVTAction("Edit District",
				ImageUtil.createIcon("telephone_edit.png"),
				"Edit the selected district.",
				KeyEvent.VK_E,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditDistrictDialog dlg = new EditDistrictDialog(getTopFrame(), district);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							//Remove the old family from the model.
							if (!districts.remove(district)){
								System.err.println("Error removing district from list.");
							}
							//Save the updated family.
							district = dlg.getDistrict();
							//Update the master model.
							districts.add(district);
							//Refresh the node display.
							updateHVTHelperNode(DistrictNode.this);
						}
					}
				});
		actionList.add(a);
		
		a = new HVTAction("Delete District",
				ImageUtil.createIcon("telephone_delete.png"),
				"Delete the selected district.",
				KeyEvent.VK_D,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						if (district.getCompanionships().size() > 0) {
							JOptionPane
									.showMessageDialog(HVTHelperNode.getTopFrame(),
											"Please delete all Companionships before deleting the district.");
						} else {
							int result = JOptionPane
									.showConfirmDialog(
											HVTHelperNode.getTopFrame(),
											"<html>Are you sure you want to delete this District?</html>",
											"Delete District?", JOptionPane.YES_NO_CANCEL_OPTION);
							if (result == JOptionPane.OK_OPTION) {
								// Update the data model.
								districts.remove(district);
								// Update the JTree model.
								removeHVTHelperNode(DistrictNode.this);
							}
						}
					}
				});
		actionList.add(a);
		
	}


	@Override
	public void populate() {
		for (Companionship c: district.getCompanionships()){
			addHVTHelperNode(new CompanionshipNode(c, district, info, unassignedNode));
		}
	}
	
	@Override
	public String[][] getTableData() {
		return new String [][] {
				{ "District Supervisor", district.getSupervisor().getName()},
				{ "Supervisor Phone", district.getSupervisor().getPhone()},
				{ "Supervisor Email", district.getSupervisor().getEmail()},
				{ "Number of Companionships", "" + district.getCompanionships().size()},
				{ "Average # of Familes", "" + getAvgNumFams()}
		};
	}
	
	private int getAvgNumFams() {
		int numCompanions = 0;
		int numFamilies = 0;
		int avg = 0;
		for (Companionship c: district.getCompanionships()){
			numCompanions++;
			numFamilies += c.getFamilies().size();
		}
		if (numCompanions > 0){
			avg = numFamilies / numCompanions;
		} 
		return avg;
	}

	@Override
	public String toString() {
		return district.getSupervisor().getName();
	}
	
	@Override
	public ImageIcon getIcon() {
			return ImageUtil.createIcon("telephone.png");
	}
	
	
}
