/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.ademille.hvthelper.gui.dialogs.EditCompFamiliesDialog;
import com.ademille.hvthelper.gui.dialogs.EditCompanionshipDialog;
import com.ademille.hvthelper.gui.util.HVTAction;
import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.Teacher;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class CompanionshipNode extends HVTHelperNode {
	
	private Companionship comp;
	private District district;
	private TeachingAssignmentInfo info;
	private HVTHelperNode unassignedNode;
	
	public CompanionshipNode(Companionship comp, District district, TeachingAssignmentInfo info, HVTHelperNode unassignedNode) {
		this.comp = comp;
		this.district = district;
		this.info = info;
		this.unassignedNode = unassignedNode;
		createActions();
	}
	
	private void createActions() {

		Action a = new HVTAction("Edit Assignments", ImageUtil.createIcon("house.png"),
				"Assign Family", KeyEvent.VK_E, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditCompFamiliesDialog dlg = new EditCompFamiliesDialog(
								getTopFrame(), comp, info);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()) {
							List<Family> added = dlg.getAddedFamilies();
							List<Family> removed = dlg.getRemovedFamiles();
							//Add new family nodes and update the underlying model.
							for (Family f:added){
								//Update the underlying model.
								comp.addFamily(f);
								//Remove the family from the unassigned list.
								info.getUnassigned().remove(f);
							}
							
							//Move unassigned families to the unassigned list and remove their nodes.
							for (Family f:removed){
								//Remove the family from this companionship.
								comp.removeFamily(f);
								//Add the family to the unassigned list.
								info.getUnassigned().add(f);
							}
							
							//Notify the tree that the unassigned nodes have changed.
							repopulateHVTHelperNode(unassignedNode);

							//Notify the tree that this node has changed and repopulate.
							repopulateHVTHelperNode(CompanionshipNode.this);
						}
					}
				});
		actionList.add(a);
		
		a = new HVTAction("Edit Companionship ", ImageUtil.createIcon("group_edit.png"),
				"Edit Companionship", KeyEvent.VK_C, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditCompanionshipDialog dlg = new EditCompanionshipDialog(getTopFrame(), comp, info);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							//Save the updated family.
							comp = dlg.getCompanionship();
							//Refresh the node display.
							updateHVTHelperNode(CompanionshipNode.this);
						}
					}
				});
		actionList.add(a);
		a = new HVTAction("Delete Companionship", ImageUtil.createIcon("group_delete.png"),
				"Delete Companionship", KeyEvent.VK_D, new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						int result = JOptionPane
								.showConfirmDialog(
										HVTHelperNode.getTopFrame(),
										"<html>Are you sure you want to delete this companionship?<br/>All Families will be moved to the 'Unassigned' list.</html>",
										"Delete Companionship?", JOptionPane.YES_NO_CANCEL_OPTION);
						if (result == JOptionPane.OK_OPTION){
							//Add all the families to the unassigned list.
							for (Family f:comp.getFamilies()){
								info.getUnassigned().add(f);
							}
							//Clear the unassigned node's children nodes.
							repopulateHVTHelperNode(unassignedNode);
							//Empty the families list.
							comp.getFamilies().clear();
							
							//Clear out the teachers
							comp.setTeacher1(null);
							comp.setTeacher2(null);
							comp.setTeacher3(null);
							
							//Remove the companionship from the district.
							district.removeCompanionship(comp);
							//Remove this Node
							removeHVTHelperNode(CompanionshipNode.this);
							
						}
					}
				});
		actionList.add(a);
	}

	@Override
	public void populate() {
		for (Family f: comp.getFamilies()){
			addHVTHelperNode(new FamilyNode(f, comp.getFamilies(),true));
		}
		
	}
	
	@Override
	public String toString() {
		return comp.joinTeachers(" - ");
	}
	
	@Override
	public String[][] getTableData() {
		Teacher t1 = comp.getTeacher1();
		Teacher t2 = comp.getTeacher2();
		Teacher t3 = comp.getTeacher3();
		String name1 = t1 == null?"": t1.getName();
		String name2 = t2 == null?"": t2.getName();
		String name3 = t3 == null?"": t3.getName();
		
		String phone1 = t1 == null?"": t1.getPhone();
		String phone2 = t2 == null?"": t2.getPhone();
		String phone3 = t3 == null?"": t3.getPhone();
		
		String email1 = t1 == null?"": t1.getEmail();
    String email2 = t2 == null?"": t2.getEmail();
    String email3 = t3 == null?"": t3.getEmail();
    
		return new String[][] {
				{"Teacher 1 Name", name1},
				{"Teacher 1 Phone ", phone1},
				{"Teacher 1 Email", email1},
				{"",""},
				{"Teacher 2 Name", name2},
				{"Teacher 2 Phone ", phone2},
				{"Teacher 2 Email", email2},
				{"",""},
				{"Teacher 3 Name", name3},
				{"Teacher 3 Phone ", phone3},
				{"Teacher 3 Email", email3},
				};
		}
	
	@Override
	public ImageIcon getIcon() {
			return ImageUtil.createIcon("group.png");
	}

}
