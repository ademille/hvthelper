/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.ademille.hvthelper.gui.dialogs.EditTeacherDialog;
import com.ademille.hvthelper.gui.util.HVTAction;
import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.Teacher;

public class TeacherNode extends HVTHelperNode {
	
	private List<Teacher> teacherList; //The list of all teachers.
	private Teacher teacher; //The specific teacher to represent.
	public TeacherNode(Teacher teacher, List<Teacher> teacherList) {
		this.teacher = teacher;
		this.teacherList = teacherList;
	}

	private void createActions(){
		Action a = new HVTAction("Edit Teacher",
				ImageUtil.createIcon("user_edit.png"),
				"Edit Teacher",
				KeyEvent.VK_E,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditTeacherDialog dlg = new EditTeacherDialog(getTopFrame(), teacher);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							//Remove the old teacher from the model.
							teacherList.remove(teacher);
							//Save the updated teacher.
							teacher = dlg.getTeacher();
							//Update the master teacher model.
							teacherList.add(teacher);
							//Refresh the node display.
							updateHVTHelperNode(TeacherNode.this);
						}
					}
				});
		actionList.add(a);	
		
		a = new HVTAction("Delete Teacher",
				ImageUtil.createIcon("user_delete.png"),
				"Delete Teacher",
				KeyEvent.VK_D,
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						List<Companionship> compList = teacher.getCompanionships();
						if (compList.isEmpty()){
							int result = JOptionPane
							.showConfirmDialog(
									HVTHelperNode.getTopFrame(),
									"<html>Are you sure you want to delete this teacher?</html>",
									"Delete Teacher?", JOptionPane.YES_NO_CANCEL_OPTION);
							if (result == JOptionPane.OK_OPTION){
								//Update the data model.
								teacherList.remove(teacher);
								//Update the JTree model.
								removeHVTHelperNode(TeacherNode.this);
							}
						} else { //The teacher is still part of a companionship.
							String compsHtml = "";
							for (Companionship c:compList){
								compsHtml += "<b>"+ c.toString() + "</b><br>";
							}
							JOptionPane.showMessageDialog(HVTHelperNode.getTopFrame(),
									"<html>This teacher is part of a companionship and can not be deleted.<br>" +
									"Remove the teacher from the following companionships first:<br><br>" +
									compsHtml + "<br>Use the Search box to find and unassign the teacher. </html>");
						}
						
					}
				});
		actionList.add(a);
	}
	
	
	@Override
	public void populate() {
		createActions();
	}
	
	@Override
	public String toString() {
		return teacher.getName();
	}
	
	@Override
	public String[][] getTableData() {
		return new String [][] {
				{ "Name", teacher.getName()},
				{ "Phone", teacher.getPhone()},
				{ "Email", teacher.getEmail()},
		};
	}
	
	@Override
	public ImageIcon getIcon() {
		return ImageUtil.createIcon("user.png");
	}


}
