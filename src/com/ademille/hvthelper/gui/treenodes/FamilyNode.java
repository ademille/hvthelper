/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.ademille.hvthelper.gui.dialogs.EditFamilyDialog;
import com.ademille.hvthelper.gui.util.HVTAction;
import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.Family;

public class FamilyNode extends HVTHelperNode {
	
	private Family family;
	private List<Family> familyList;
	private boolean assigned; //Is this family assigned to a companionship?
	public FamilyNode(Family family, List<Family> familyList, boolean assigned) {
		this.assigned = assigned;
		this.family = family;
		this.familyList = familyList;
		createActions();
	}
	
	private void createActions(){
		Action a = new HVTAction("Edit Family",
				ImageUtil.createIcon("house_edit.png"),
				"Edit Family",
				new Integer(KeyEvent.VK_E),
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditFamilyDialog dlg = new EditFamilyDialog(getTopFrame(), family);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							//Remove the old family from the model.
							familyList.remove(family);
							//Save the updated family.
							family = dlg.getFamily();
							//Update the master model.
							familyList.add(family);
							//Refresh the node display.
							updateHVTHelperNode(FamilyNode.this);
						}
					}
				});
		actionList.add(a);	
		//Don't allow deleting the family unless it's an unassigned family.
		if (!assigned) {
			a = new HVTAction("Delete Family", ImageUtil
					.createIcon("house_delete.png"), "Delete Family", new Integer(
					KeyEvent.VK_D), new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (JOptionPane.showConfirmDialog(HVTHelperNode.getTopFrame(),
							"Are you sure you want to delete this family?") == JOptionPane.OK_OPTION) {
						// Update the data model.
						familyList.remove(family);
						// Update the JTree model.
						removeHVTHelperNode(FamilyNode.this);
					}
				}
			});
			actionList.add(a);
		} 
		
	}
	
	
	@Override
	public void populate() {
	}
	
	@Override
	public String toString() {
		return family.getName();
	}
	
	@Override
	public String[][] getTableData() {
		return new String [][] {
				{ "Name", family.getName()},
				{ "Phone", family.getPhone1()},
				{ "Address", family.getStreet1()},
				{ "Email", family.getEmail()}
		};
	}
	
	@Override
	public ImageIcon getIcon() {
		return ImageUtil.createIcon("house.png");
	}


}
