/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;

import com.ademille.hvthelper.gui.dialogs.EditTeacherDialog;
import com.ademille.hvthelper.gui.util.HVTAction;
import com.ademille.hvthelper.gui.util.ImageUtil;
import com.ademille.hvthelper.model.Teacher;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class TeacherListNode extends HVTHelperNode{
	private boolean homeTeaching = true;
	List<Teacher> teachers = null;
	TeachingAssignmentInfo info;
	
	public TeacherListNode(TeachingAssignmentInfo info) {
		this.info = info;
		this.teachers = info.getTeachers();
		this.homeTeaching = info.isHometeaching();
	}
	
	private void createActions(){
		Action a = new HVTAction("Add Teacher",
				ImageUtil.createIcon("user.png"),
				"Add a New Teacher",
				new Integer(KeyEvent.VK_A),
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						EditTeacherDialog dlg = new EditTeacherDialog(getTopFrame(), null);
						dlg.setModal(true);
						dlg.setVisible(true);
						if (!dlg.isCancelled()){
							Teacher t = dlg.getTeacher();
							teachers.add(t);
							//Figure out where the item was added, so we can add a GUI node at the same
							//place. This will make sure the tree reflects the actual data structure.
							int pos = teachers.indexOf(t);
							addHVTHelperNode(new TeacherNode(t,teachers),pos);
						}
					}
				});
		actionList.add(a);	
	}
	
	@Override
	public String toString() {
		return "Teachers";
	}

	@Override
	public void populate() {
		if (null != teachers) {
			for (Teacher t : teachers) {
				//If the teacher doesn't have a name, then don't add it.
				//It is probably a companionship with only one teacher.
				if (null != t && !t.getName().trim().equals("")){
					addHVTHelperNode(new TeacherNode(t,teachers));
				}
			}
		}
		createActions();
	}
	
	@Override
	public ImageIcon getIcon() {
		return homeTeaching ? ImageUtil.createIcon("user.png") : ImageUtil.createIcon("user_female.png");
	}

}
