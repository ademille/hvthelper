/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.treenodes;

import java.util.LinkedList;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

public abstract class HVTHelperNode extends DefaultMutableTreeNode implements Comparable<HVTHelperNode>{

	protected List<Action> actionList;
	//Create a static variable to hold the tree model.  Otherwise, we'd have to pass
	//the model to every node so it could update the jtree if necessary.
	private static DefaultTreeModel model;
	
	//Keep a copy of the tree so that when nodes are inserted and deleted, a node can be selected.
	private static JTree tree;
	
	private static JFrame topFrame; //The top GUI frame.  Used to center dialogs.
	
	private static DirtyListener dirtyListener;
	
	public HVTHelperNode() {
		actionList = new LinkedList<Action>();
	}
	
	 // Returns a TreePath containing the specified node.
  public static TreePath getPath(TreeNode node) {
      LinkedList<TreeNode> list = new LinkedList<TreeNode>();
  
      // Add all nodes to list
      while (node != null) {
          list.addFirst(node);
          node = node.getParent();
      }
      // Convert array of nodes to TreePath
      if (list.size()>0){
      	return new TreePath(list.toArray());
      } else {
      	return null;
      }
  }


	protected void addHVTHelperNode(HVTHelperNode node) {
  	add(node);
  	populateNode(node);
  }
  
	protected void addHVTHelperNode(HVTHelperNode node, int insertPos) {
		insert(node, insertPos);
		populateNode(node);
		
	}
	
	private void populateNode(HVTHelperNode node) {
		node.populate();
		if (null != model) {
			int index = this.getIndex(node);
			model.nodesWereInserted(this, new int[] { index });
		}
		setDirty(true);
		// Make the new node visible, but don't select it.
		// if (null!=tree){
		// tree.scrollPathToVisible(getPath(node));
		// }
	}
	
	//Child nodes can call this if the model has been changed.
	protected void updateHVTHelperNode(HVTHelperNode node){
		model.nodeChanged(node);
		if (null!=tree){
			//Re-select the node to force the panel to update.
			tree.getSelectionModel().clearSelection();
			tree.setSelectionPath(getPath(node));
		}
		setDirty(true);
	}
	
//Child nodes can call this if the model has been changed.
	protected void updateHVTHelperNodeChildren(HVTHelperNode node){
		model.nodeStructureChanged(node);
		if (null!=tree){
			//Re-select the node to force the panel to update.
			tree.getSelectionModel().clearSelection();
			tree.setSelectionPath(getPath(node));
		}
		setDirty(true);
	}
	
	//Child nodes can call this to remove a node from the tree.
	protected void removeHVTHelperNode(HVTHelperNode node){
		DefaultMutableTreeNode parent = (DefaultMutableTreeNode)node.getParent();
		if (parent!=null){
			int index = parent.getIndex(node);
			parent.remove(node);
			model.nodesWereRemoved(parent, new int[]{index}, new Object[]{node});
			if (null!=tree){
				tree.setSelectionPath(getPath(parent));
			}
    }
		setDirty(true);
	}
	
	protected void repopulateHVTHelperNode(HVTHelperNode node){
		if (node.children!=null){
			node.children.clear();
		}
		node.populate();
		updateHVTHelperNodeChildren(node);
	}

	public abstract void populate();

	public String[][] getTableData() {
		return new String[][] { { "No Data" } };
	}

	public String[] getColumnNames() {
		return new String[] { "Property", "Value" };
	}

	@Override
	public Object getUserObject() {
		// This is to allow the sorting wrapper to work correctly.
		return this;
	}
	
	/**
	 * This will be called by the GUI to collect a list of Actions that 
	 * are applicable for this type of node.  Children objects
	 * should return an appropriate list of actions, such as actions
	 * for creating child nodes, etc.  The actions will be displayed
	 * as buttons in the button bar and as context menu items.
	 * @return
	 */
	public List<Action> getActionList(){
		return actionList;
	}

	/**
	 * 
	 * @return The ImageIcon to be displayed in the JTree.
	 */
	public ImageIcon getIcon() {
		return null;
	}

	/**
	 * @return the model
	 */
	public static DefaultTreeModel getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	public static void setModel(DefaultTreeModel model) {
		HVTHelperNode.model = model;
	}

	/**
	 * @return the topFrame
	 */
	public static JFrame getTopFrame() {
		return topFrame;
	}

	/**
	 * @param topFrame the topFrame to set
	 */
	public static void setTopFrame(JFrame topFrame) {
		HVTHelperNode.topFrame = topFrame;
	}

	/**
	 * @return the tree
	 */
	public static JTree getTree() {
		return tree;
	}

	/**
	 * @param tree the tree to set
	 */
	public static void setTree(JTree tree) {
		HVTHelperNode.tree = tree;
	}
	
	@Override
	public int compareTo(HVTHelperNode node) {
		return this.toString().toLowerCase().compareTo(node.toString().toLowerCase());
	}

	protected static void setDirty(boolean dirty) {
		if (null!=dirtyListener){
			dirtyListener.setDirty(dirty);
		}
	}
	
	/**
	 * @param dirtyListener - A listener for dirty data.
	 */
	public static void setDirtyListener(DirtyListener dirtyListener) {
		HVTHelperNode.dirtyListener = dirtyListener;
	}
	
}
