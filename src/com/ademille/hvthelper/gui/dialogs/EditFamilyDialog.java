package com.ademille.hvthelper.gui.dialogs;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Frame;
import javax.swing.JDialog;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.JButton;

import com.ademille.hvthelper.gui.util.CenterDialog;
import com.ademille.hvthelper.model.Family;

public class EditFamilyDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel content = null;

	private JLabel lblName = null;

	private JLabel lblPhone = null;

	private JTextField txtName = null;

	private JTextField txtPhone = null;

	private JPanel panelButtons = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JPanel panelSpacer = null;
	
	private Family family = null;  //  @jve:decl-index=0:
	private boolean cancelled = true;

	private JLabel lblEmail = null;

	private JTextField txtEmail = null;

	private JLabel lblStreet = null;

	private JTextField txtStreet = null;

	private JLabel lblState = null;

	private JLabel lblCity = null;

	private JTextField txtCity = null;

	private JTextField txtState = null;

	
	public EditFamilyDialog(Frame owner, Family family){
		super(owner);
		initialize();
		if (null == family){
		//No teacher provided so create a new one.
			this.family = new Family();
			setTitle("New Family");
		} else {
			//A teacher object was provided, so we will edit it.
			this.family = family;
			txtName.setText(family.getName());
			txtPhone.setText(family.getPhone1());
			txtEmail.setText(family.getEmail());
			txtStreet.setText(family.getStreet1());
			txtCity.setText(family.getCity());
			txtState.setText(family.getState());
			setTitle("Edit Family");
		}
		CenterDialog.centerDialog(owner, this);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(345, 308);
		this.setContentPane(getContent());
	}
	
	/**
	 * This method initializes content
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getContent() {
		if (content == null) {
			GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
			gridBagConstraints8.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints8.gridy = 6;
			gridBagConstraints8.weightx = 1.0;
			gridBagConstraints8.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints8.gridx = 1;
			GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
			gridBagConstraints7.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints7.gridy = 4;
			gridBagConstraints7.weightx = 1.0;
			gridBagConstraints7.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints7.gridx = 1;
			GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
			gridBagConstraints6.gridx = 0;
			gridBagConstraints6.anchor = GridBagConstraints.WEST;
			gridBagConstraints6.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints6.gridy = 4;
			lblCity = new JLabel();
			lblCity.setText("City:");
			GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
			gridBagConstraints5.gridx = 0;
			gridBagConstraints5.anchor = GridBagConstraints.WEST;
			gridBagConstraints5.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints5.gridy = 6;
			lblState = new JLabel();
			lblState.setText("State:");
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints4.gridy = 3;
			gridBagConstraints4.weightx = 1.0;
			gridBagConstraints4.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints4.gridx = 1;
			GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
			gridBagConstraints31.gridx = 0;
			gridBagConstraints31.anchor = GridBagConstraints.WEST;
			gridBagConstraints31.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints31.gridy = 3;
			lblStreet = new JLabel();
			lblStreet.setText("Street:");
			GridBagConstraints gridBagConstraints22 = new GridBagConstraints();
			gridBagConstraints22.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints22.gridy = 2;
			gridBagConstraints22.weightx = 1.0;
			gridBagConstraints22.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints22.gridx = 1;
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.gridx = 0;
			gridBagConstraints12.anchor = GridBagConstraints.WEST;
			gridBagConstraints12.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints12.gridy = 2;
			lblEmail = new JLabel();
			lblEmail.setText("Email:");
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.gridx = 0;
			gridBagConstraints21.fill = GridBagConstraints.BOTH;
			gridBagConstraints21.gridwidth = 2;
			gridBagConstraints21.weighty = 1.0D;
			gridBagConstraints21.gridy = 7;
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 0;
			gridBagConstraints11.fill = GridBagConstraints.BOTH;
			gridBagConstraints11.gridwidth = 2;
			gridBagConstraints11.gridheight = 1;
			gridBagConstraints11.weighty = 0.0D;
			gridBagConstraints11.gridy = 8;
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints3.gridy = 1;
			gridBagConstraints3.weightx = 1.0;
			gridBagConstraints3.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints3.gridx = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints2.gridy = 0;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints2.gridx = 1;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.anchor = GridBagConstraints.WEST;
			gridBagConstraints1.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints1.gridy = 0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.anchor = GridBagConstraints.WEST;
			gridBagConstraints.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints.gridy = 1;
			lblPhone = new JLabel();
			lblPhone.setText("Phone:");
			lblName = new JLabel();
			lblName.setText("Name:");
			content = new JPanel();
			content.setLayout(new GridBagLayout());
			content.add(lblName, gridBagConstraints1);
			content.add(lblPhone, gridBagConstraints);
			content.add(getTxtName(), gridBagConstraints2);
			content.add(getTxtPhone(), gridBagConstraints3);
			content.add(getPanelButtons(), gridBagConstraints11);
			content.add(getPanelSpacer(), gridBagConstraints21);
			content.add(lblEmail, gridBagConstraints12);
			content.add(getTxtEmail(), gridBagConstraints22);
			content.add(lblStreet, gridBagConstraints31);
			content.add(getTxtStreet(), gridBagConstraints4);
			content.add(lblState, gridBagConstraints5);
			content.add(getTxtCity(), gridBagConstraints7);
			content.add(lblCity, gridBagConstraints6);
			content.add(getTxtState(), gridBagConstraints8);
		}
		return content;
	}

	/**
	 * This method initializes txtName	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtName() {
		if (txtName == null) {
			txtName = new JTextField();
		}
		return txtName;
	}

	/**
	 * This method initializes txtPhone	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtPhone() {
		if (txtPhone == null) {
			txtPhone = new JTextField();
		}
		return txtPhone;
	}

	/**
	 * This method initializes panelButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setHgap(15);
			panelButtons = new JPanel();
			panelButtons.setLayout(flowLayout);
			panelButtons.add(getBtnOk());
			panelButtons.add(getBtnCancel());
		}
		return panelButtons;
	}

	/**
	 * This method initializes btnOk	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("OK");
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String name = txtName.getText().trim();
					if ("".equals(name)){
						JOptionPane.showMessageDialog(EditFamilyDialog.this,
								"Please enter a Name.", "Missing Name",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						family.setName(name);
						family.setPhone1(txtPhone.getText().trim());
						family.setEmail(txtEmail.getText().trim());
						family.setStreet1(txtStreet.getText().trim());
						family.setCity(txtCity.getText().trim());
						family.setState(txtState.getText().trim());
						cancelled = false;
						setVisible(false);
					}
				}
			});
		}
		return btnOk;
	}

	/**
	 * This method initializes btnCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = true;
					setVisible(false);
				}
			});
		}
		return btnCancel;
	}

	/**
	 * This method initializes panelSpacer	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelSpacer() {
		if (panelSpacer == null) {
			panelSpacer = new JPanel();
			panelSpacer.setLayout(new GridBagLayout());
		}
		return panelSpacer;
	}
	
	/**
	 * @return the cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * @return the teacher
	 */
	public Family getFamily() {
		return family;
	}

	/**
	 * @param teacher the teacher to set
	 */
	public void setFamily(Family family) {
		this.family = family;
	}

	/**
	 * This method initializes txtEmail	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtEmail() {
		if (txtEmail == null) {
			txtEmail = new JTextField();
		}
		return txtEmail;
	}

	/**
	 * This method initializes txtStreet	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtStreet() {
		if (txtStreet == null) {
			txtStreet = new JTextField();
		}
		return txtStreet;
	}

	/**
	 * This method initializes txtCity	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtCity() {
		if (txtCity == null) {
			txtCity = new JTextField();
		}
		return txtCity;
	}

	/**
	 * This method initializes txtState	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtState() {
		if (txtState == null) {
			txtState = new JTextField();
		}
		return txtState;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
