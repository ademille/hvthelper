/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ademille.hvthelper.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import com.ademille.hvthelper.gui.util.CenterDialog;

public class HTVTSelectionDialog extends JDialog {

	private boolean cancelled = false;

	private static final long serialVersionUID = 1L;

	private JPanel jContentPane = null;

	private JPanel panelOptions = null;

	private JLabel lblDescription = null;

	private JPanel panelButtons = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JRadioButton radioHT = null;

	private JRadioButton radioVT = null;

	private ButtonGroup buttonGroup = null;

	/**
	 * @param owner
	 */
	public HTVTSelectionDialog(Frame owner) {
		super(owner);
		initialize();
		CenterDialog.centerDialog(owner, this);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(344, 178);
		this.setTitle("Report Options");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			BorderLayout borderLayout = new BorderLayout();
			borderLayout.setHgap(5);
			borderLayout.setVgap(5);
			lblDescription = new JLabel();
			lblDescription.setText("Select the group to include in the report.");
			lblDescription.setHorizontalAlignment(SwingConstants.CENTER);
			lblDescription.setFont(new Font("Dialog", Font.BOLD, 14));
			lblDescription.setHorizontalTextPosition(SwingConstants.CENTER);
			jContentPane = new JPanel();
			jContentPane.setLayout(borderLayout);
			jContentPane.add(getPanelOptions(), BorderLayout.CENTER);
			jContentPane.add(lblDescription, BorderLayout.NORTH);
			jContentPane.add(getPanelButtons(), BorderLayout.SOUTH);
		}
		return jContentPane;
	}

	/**
	 * This method initializes panelOptions	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelOptions() {
		if (panelOptions == null) {
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 1;
			gridBagConstraints.weighty = 0.0D;
			gridBagConstraints.anchor = GridBagConstraints.WEST;
			gridBagConstraints.gridy = 1;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 1;
			gridBagConstraints1.anchor = GridBagConstraints.WEST;
			gridBagConstraints1.gridy = 0;
			panelOptions = new JPanel();
			panelOptions.setLayout(new GridBagLayout());
			panelOptions.add(getRadioHT(), gridBagConstraints1);
			panelOptions.add(getRadioVT(), gridBagConstraints);
			buttonGroup = new ButtonGroup();
			buttonGroup.add(radioHT);
			buttonGroup.add(radioVT);
		}
		return panelOptions;
	}

	/**
	 * This method initializes panelButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setHgap(15);
			panelButtons = new JPanel();
			panelButtons.setLayout(flowLayout);
			panelButtons.add(getBtnOk(), null);
			panelButtons.add(getBtnCancel(), null);
		}
		return panelButtons;
	}

	/**
	 * This method initializes btnOk	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("Ok");
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = false;
					setVisible(false);
				}
			});
		}
		return btnOk;
	}

	/**
	 * This method initializes btnCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = true;
					setVisible(false);
				}
			});
		}
		return btnCancel;
	}
	
	public boolean isHomeTeaching(){
		return radioHT.isSelected();
	}
	

	public boolean isCancelled() {
		return cancelled;
	}

  /**
   * This method initializes radioHT	
   * 	
   * @return javax.swing.JRadioButton	
   */
  private JRadioButton getRadioHT() {
    if (radioHT == null) {
      radioHT = new JRadioButton();
      radioHT.setText("Home Teaching");
      radioHT.setSelected(true);
    }
    return radioHT;
  }

  /**
   * This method initializes radioVT	
   * 	
   * @return javax.swing.JRadioButton	
   */
  private JRadioButton getRadioVT() {
    if (radioVT == null) {
      radioVT = new JRadioButton();
      radioVT.setText("Visiting Teaching");
    }
    return radioVT;
  }

}  //  @jve:decl-index=0:visual-constraint="10,10"
