/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.dialogs;

import com.ademille.hvthelper.model.Teacher;

/**
 * This class acts as a Decorator for a teacher object.
 * It is really only intended to be used with a JCombo. It will
 * change the toString() method to add on an appropriate string
 * if the teacher is already assigned to a companionship.  
 * @author ademille
 *
 */
public class TeacherAssignmentDecorator implements Comparable<TeacherAssignmentDecorator> {
	//The teacher that will be decorated.
	private Teacher teacher;
	
	public TeacherAssignmentDecorator(Teacher teacher){
		this.teacher = teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Teacher getTeacher() {
		return teacher;
	}
	
	@Override
	public String toString() {
		if (null!=teacher){
			//Check is this teacher is part of a companionship.
			if (teacher.getCompanionships().size()>0){
				return "* " + teacher.toString(); 
			} else {
				return teacher.toString();
			}
		} else {
			return "";
		}
	}
	
	public int compareTo(TeacherAssignmentDecorator t) {
		return teacher.getName().compareTo(t.getTeacher().getName());
	}

}
