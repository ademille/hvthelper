package com.ademille.hvthelper.gui.dialogs;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Frame;
import javax.swing.JDialog;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.JButton;

import com.ademille.hvthelper.gui.util.CenterDialog;
import com.ademille.hvthelper.model.Teacher;

public class EditTeacherDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel content = null;

	private JLabel lblName = null;

	private JLabel lblPhone = null;

	private JTextField txtName = null;

	private JTextField txtPhone = null;

	private JPanel panelButtons = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JPanel panelSpacer = null;
	
	private Teacher teacher = null;  //  @jve:decl-index=0:
	private boolean cancelled = true;

  private JTextField txtEmail = null;

  private JLabel lblEmail = null;

	
	public EditTeacherDialog(Frame owner, Teacher teacher){
		super(owner);
		initialize();
		if (null == teacher){
		//No teacher provided so create a new one.
			this.teacher = new Teacher("","","");
			setTitle("New Teacher");
		} else {
			//A teacher object was provided, so we will edit it.
			this.teacher = teacher;
			txtName.setText(teacher.getName());
			txtPhone.setText(teacher.getPhone());
			txtEmail.setText(teacher.getEmail());
			setTitle("Edit Teacher");
		}
		
		CenterDialog.centerDialog(owner, this);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(300, 171);
		this.setTitle("Edit Teacher");
		this.setContentPane(getContent());
	}
	
	

	/**
	 * This method initializes content
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getContent() {
		if (content == null) {
			GridBagConstraints gridBagConstraints22 = new GridBagConstraints();
			gridBagConstraints22.gridx = 0;
			gridBagConstraints22.anchor = GridBagConstraints.NORTHWEST;
			gridBagConstraints22.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints22.gridy = 2;
			lblEmail = new JLabel();
			lblEmail.setText("Email:");
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints12.gridy = 2;
			gridBagConstraints12.weightx = 1.0;
			gridBagConstraints12.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints12.gridx = 1;
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.gridx = 0;
			gridBagConstraints21.fill = GridBagConstraints.BOTH;
			gridBagConstraints21.gridwidth = 2;
			gridBagConstraints21.weighty = 1.0D;
			gridBagConstraints21.gridy = 3;
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 0;
			gridBagConstraints11.fill = GridBagConstraints.BOTH;
			gridBagConstraints11.gridwidth = 2;
			gridBagConstraints11.gridheight = 1;
			gridBagConstraints11.weighty = 0.0D;
			gridBagConstraints11.gridy = 4;
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints3.gridy = 1;
			gridBagConstraints3.weightx = 1.0;
			gridBagConstraints3.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints3.gridx = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints2.gridy = 0;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints2.gridx = 1;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints1.gridy = 0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
			gridBagConstraints.gridy = 1;
			lblPhone = new JLabel();
			lblPhone.setText("Phone:");
			lblName = new JLabel();
			lblName.setText("Name:");
			content = new JPanel();
			content.setLayout(new GridBagLayout());
			content.add(lblName, gridBagConstraints1);
			content.add(lblPhone, gridBagConstraints);
			content.add(getTxtName(), gridBagConstraints2);
			content.add(getTxtPhone(), gridBagConstraints3);
			content.add(getPanelButtons(), gridBagConstraints11);
			content.add(getPanelSpacer(), gridBagConstraints21);
			content.add(getTxtEmail(), gridBagConstraints12);
			content.add(lblEmail, gridBagConstraints22);
		}
		return content;
	}

	/**
	 * This method initializes txtName	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtName() {
		if (txtName == null) {
			txtName = new JTextField();
		}
		return txtName;
	}

	/**
	 * This method initializes txtPhone	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtPhone() {
		if (txtPhone == null) {
			txtPhone = new JTextField();
		}
		return txtPhone;
	}

	/**
	 * This method initializes panelButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setHgap(15);
			panelButtons = new JPanel();
			panelButtons.setLayout(flowLayout);
			panelButtons.add(getBtnOk());
			panelButtons.add(getBtnCancel());
		}
		return panelButtons;
	}

	/**
	 * This method initializes btnOk	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("OK");
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String n = txtName.getText().trim();
					String p = txtPhone.getText().trim();
					String em = txtEmail.getText().trim();
					if ("".equals(n)){
						JOptionPane.showMessageDialog(EditTeacherDialog.this,
								"Please enter a Name.", "Missing Name",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						teacher.setName(n);
						teacher.setPhone(p);
						teacher.setEmail(em);
						cancelled = false;
						setVisible(false);
					}
				}
			});
		}
		return btnOk;
	}

	/**
	 * This method initializes btnCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = true;
					setVisible(false);
				}
			});
		}
		return btnCancel;
	}

	/**
	 * This method initializes panelSpacer	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelSpacer() {
		if (panelSpacer == null) {
			panelSpacer = new JPanel();
			panelSpacer.setLayout(new GridBagLayout());
		}
		return panelSpacer;
	}
	
	/**
	 * @return the cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * @return the teacher
	 */
	public Teacher getTeacher() {
		return teacher;
	}

	/**
	 * @param teacher the teacher to set
	 */
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

  /**
   * This method initializes txtEmail	
   * 	
   * @return javax.swing.JTextField	
   */
  private JTextField getTxtEmail() {
    if (txtEmail == null) {
      txtEmail = new JTextField();
    }
    return txtEmail;
  }

}  //  @jve:decl-index=0:visual-constraint="10,10"
