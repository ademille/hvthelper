package com.ademille.hvthelper.gui.dialogs;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Frame;
import javax.swing.JDialog;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
import java.awt.FlowLayout;
import javax.swing.JButton;

import com.ademille.hvthelper.gui.util.CenterDialog;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Supervisor;

public class EditDistrictDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel content = null;

	private JLabel lblName = null;

	private JLabel lblPhone = null;

	private JTextField txtName = null;

	private JTextField txtPhone = null;

	private JPanel panelButtons = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JPanel panelSpacer = null;
	
	private District district= null;  //  @jve:decl-index=0:
	private Supervisor supervisor = null;
	private boolean cancelled = true;

  private JTextField txtEmail = null;

  private JLabel lblEmail = null;

	public EditDistrictDialog(Frame owner, District district){
		super(owner);
		initialize();
		if (null == district){
		//No teacher provided so create a new one.
			this.supervisor = new Supervisor("","","");
			this.district = new District(supervisor);
			setTitle("New District Supervisor");
		} else {
			//A teacher object was provided, so we will edit it.
			this.district = district;
			this.supervisor = district.getSupervisor();
			txtName.setText(district.getSupervisor().getName());
			txtPhone.setText(district.getSupervisor().getPhone());
			txtEmail.setText(district.getSupervisor().getEmail());
			setTitle("Edit District Supervisor");
		}
		CenterDialog.centerDialog(owner, this);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(345, 165);
		this.setContentPane(getContent());
	}
	
	/**
	 * This method initializes content
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getContent() {
		if (content == null) {
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.gridx = 0;
			gridBagConstraints4.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints4.anchor = GridBagConstraints.NORTHWEST;
			gridBagConstraints4.gridy = 2;
			lblEmail = new JLabel();
			lblEmail.setText("Email:");
			GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
			gridBagConstraints31.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints31.gridy = 2;
			gridBagConstraints31.weightx = 1.0;
			gridBagConstraints31.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints31.gridx = 1;
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.gridx = 0;
			gridBagConstraints21.fill = GridBagConstraints.BOTH;
			gridBagConstraints21.gridwidth = 2;
			gridBagConstraints21.weighty = 1.0D;
			gridBagConstraints21.gridy = 7;
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 0;
			gridBagConstraints11.fill = GridBagConstraints.BOTH;
			gridBagConstraints11.gridwidth = 2;
			gridBagConstraints11.gridheight = 1;
			gridBagConstraints11.weighty = 0.0D;
			gridBagConstraints11.gridy = 8;
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints3.gridy = 1;
			gridBagConstraints3.weightx = 1.0;
			gridBagConstraints3.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints3.gridx = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints2.gridy = 0;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints2.gridx = 1;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.anchor = GridBagConstraints.WEST;
			gridBagConstraints1.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints1.gridy = 0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.anchor = GridBagConstraints.WEST;
			gridBagConstraints.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints.gridy = 1;
			lblPhone = new JLabel();
			lblPhone.setText("Supervisor Phone:");
			lblName = new JLabel();
			lblName.setText("Supervisor Name:");
			content = new JPanel();
			content.setLayout(new GridBagLayout());
			content.add(lblName, gridBagConstraints1);
			content.add(lblPhone, gridBagConstraints);
			content.add(getTxtName(), gridBagConstraints2);
			content.add(getTxtPhone(), gridBagConstraints3);
			content.add(getPanelButtons(), gridBagConstraints11);
			content.add(getPanelSpacer(), gridBagConstraints21);
			content.add(getTxtEmail(), gridBagConstraints31);
			content.add(lblEmail, gridBagConstraints4);
		}
		return content;
	}

	/**
	 * This method initializes txtName	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtName() {
		if (txtName == null) {
			txtName = new JTextField();
		}
		return txtName;
	}

	/**
	 * This method initializes txtPhone	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTxtPhone() {
		if (txtPhone == null) {
			txtPhone = new JTextField();
		}
		return txtPhone;
	}

	/**
	 * This method initializes panelButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setHgap(15);
			panelButtons = new JPanel();
			panelButtons.setLayout(flowLayout);
			panelButtons.add(getBtnOk());
			panelButtons.add(getBtnCancel());
		}
		return panelButtons;
	}

	/**
	 * This method initializes btnOk	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("OK");
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String name = txtName.getText().trim();
					if ("".equals(name)){
						JOptionPane.showMessageDialog(EditDistrictDialog.this,
								"Please enter a Name.", "Missing Name",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						supervisor.setName(name);
						supervisor.setPhone(txtPhone.getText().trim());
						supervisor.setEmail(txtEmail.getText().trim());
						cancelled = false;
						setVisible(false);
					}
				}
			});
		}
		return btnOk;
	}

	/**
	 * This method initializes btnCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = true;
					setVisible(false);
				}
			});
		}
		return btnCancel;
	}

	/**
	 * This method initializes panelSpacer	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelSpacer() {
		if (panelSpacer == null) {
			panelSpacer = new JPanel();
			panelSpacer.setLayout(new GridBagLayout());
		}
		return panelSpacer;
	}
	
	/**
	 * @return the cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * @return the teacher
	 */
	public District getDistrict() {
		return district;
	}

	/**
	 * @param teacher the teacher to set
	 */
	public void setDistrict(District district) {
		this.district = district;
	}

  /**
   * This method initializes txtEmail	
   * 	
   * @return javax.swing.JTextField	
   */
  private JTextField getTxtEmail() {
    if (txtEmail == null) {
      txtEmail = new JTextField();
    }
    return txtEmail;
  }

}  //  @jve:decl-index=0:visual-constraint="10,10"
