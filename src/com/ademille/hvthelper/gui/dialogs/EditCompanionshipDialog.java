package com.ademille.hvthelper.gui.dialogs;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Frame;
import javax.swing.JDialog;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JButton;

import com.ademille.hvthelper.gui.treenodes.HVTHelperNode;
import com.ademille.hvthelper.gui.util.CenterDialog;
import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.Teacher;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;
import javax.swing.JComboBox;
import java.awt.BorderLayout;

public class EditCompanionshipDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel content = null;

	private JLabel lblComp1 = null;

	private JLabel lblComp2 = null;

	private JPanel panelButtons = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JPanel panelSpacer = null;
	
	private Companionship comp= null;  //  @jve:decl-index=0:
	private TeacherAssignmentDecorator noTeacher;
	private boolean cancelled = true;

	private JComboBox comboTeacher1 = null;

	private JComboBox comboTeacher2 = null;

	private JComboBox comboTeacher3 = null;

	private JLabel lblComp3 = null;

	private JLabel lblNote = null;
	
	

	public EditCompanionshipDialog(Frame owner, Companionship comp, TeachingAssignmentInfo info){
		super(owner);
		initialize();
		//Create an empty teacher to add to the list.
		noTeacher = new TeacherAssignmentDecorator(new Teacher("None","",""));
		if (null == comp){
			setTitle("New Companionship");
			this.comp = new Companionship();
		} else {
			setTitle("Edit Companionship");
			this.comp = comp;
		}
		
		CenterDialog.centerDialog(owner, this);
		
		//Setup the drop down lists for selecting a teacher.
		DefaultComboBoxModel m1;
		DefaultComboBoxModel m2;
		DefaultComboBoxModel m3;
		Vector<TeacherAssignmentDecorator> teachers = new Vector<TeacherAssignmentDecorator>();
		for (Teacher t: info.getTeachers()){
			teachers.add(new TeacherAssignmentDecorator(t));
		}
		Collections.sort(teachers);
		teachers.add(0, noTeacher);
		m1 = new DefaultComboBoxModel(teachers);
		m2 = new DefaultComboBoxModel(teachers);
		m3 = new DefaultComboBoxModel(teachers);
		comboTeacher1.setModel(m1);
		comboTeacher2.setModel(m2);
		comboTeacher3.setModel(m3);
		
		//Try to select the appropriate teacher in the list.
		if (null != comp){
			Teacher t1 = comp.getTeacher1();
			Teacher t2 = comp.getTeacher2();
			Teacher t3 = comp.getTeacher3();
			setSelectedTeacher(t1,teachers,comboTeacher1);
			setSelectedTeacher(t2,teachers,comboTeacher2);
			setSelectedTeacher(t3,teachers,comboTeacher3);
		}
	}
	
	private static void setSelectedTeacher(Teacher t, Vector<TeacherAssignmentDecorator> teachers, JComboBox combo){
		if (t !=null){
			int pos = -1;
			//Find the position of the teacher in the list.
			for (int i = 0; i < teachers.size();i++){
				if (teachers.get(i).getTeacher().equals(t)){
					pos = i;
					break;
				}
			}
			if (pos >= 0){ 
				combo.setSelectedIndex(pos); 
			}
			else {
				deletedTeacherMsg(t.getName());
			}
		}
	}

	private static void deletedTeacherMsg(String name) {
		JOptionPane.showMessageDialog(HVTHelperNode.getTopFrame(), 
				"<html>The assigned teacher, " + name + ", has been deleted from the teacher list.<br>" + 
				"You need to assign a different teacher or re-add the teacher's information.</html>","Invalid Teacher",JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(345, 209);
		this.setContentPane(getContent());
	}
	
	/**
	 * This method initializes content
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getContent() {
		if (content == null) {
			GridBagConstraints gridBagConstraints22 = new GridBagConstraints();
			gridBagConstraints22.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints22.gridy = 2;
			gridBagConstraints22.weightx = 1.0;
			gridBagConstraints22.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints22.gridx = 1;
			GridBagConstraints gridBagConstraints13 = new GridBagConstraints();
			gridBagConstraints13.gridx = 0;
			gridBagConstraints13.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints13.gridy = 2;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints2.gridy = 1;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints2.gridx = 1;
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.fill = GridBagConstraints.HORIZONTAL;
			gridBagConstraints12.gridy = 0;
			gridBagConstraints12.weightx = 1.0;
			gridBagConstraints12.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints12.gridx = 1;
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.gridx = 0;
			gridBagConstraints21.fill = GridBagConstraints.BOTH;
			gridBagConstraints21.gridwidth = 2;
			gridBagConstraints21.weighty = 1.0D;
			gridBagConstraints21.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints21.gridy = 7;
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 0;
			gridBagConstraints11.fill = GridBagConstraints.BOTH;
			gridBagConstraints11.gridwidth = 2;
			gridBagConstraints11.gridheight = 1;
			gridBagConstraints11.weighty = 0.0D;
			gridBagConstraints11.gridy = 8;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.anchor = GridBagConstraints.WEST;
			gridBagConstraints1.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints1.gridy = 0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.anchor = GridBagConstraints.WEST;
			gridBagConstraints.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints.gridy = 1;
			lblComp3 = new JLabel();
			lblComp3.setText("Teacher 3:");
			lblComp2 = new JLabel();
			lblComp2.setText("Teacher 2:");
			lblComp1 = new JLabel();
			lblComp1.setText("Teacher 1:");
			content = new JPanel();
			content.setLayout(new GridBagLayout());
			content.add(lblComp1, gridBagConstraints1);
			content.add(lblComp2, gridBagConstraints);
			content.add(lblComp3, gridBagConstraints13);
			content.add(getComboTeacher3(), gridBagConstraints22);
			content.add(getPanelButtons(), gridBagConstraints11);
			content.add(getPanelSpacer(), gridBagConstraints21);
			content.add(getComboTeacher1(), gridBagConstraints12);
			content.add(getComboTeacher2(), gridBagConstraints2);
		}
		return content;
	}

	/**
	 * This method initializes panelButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setHgap(15);
			panelButtons = new JPanel();
			panelButtons.setLayout(flowLayout);
			panelButtons.add(getBtnOk());
			panelButtons.add(getBtnCancel());
		}
		return panelButtons;
	}

	/**
	 * This method initializes btnOk	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("OK");
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					TeacherAssignmentDecorator t1 = (TeacherAssignmentDecorator)comboTeacher1.getSelectedItem();					
					TeacherAssignmentDecorator t2 = (TeacherAssignmentDecorator)comboTeacher2.getSelectedItem();
					TeacherAssignmentDecorator t3 = (TeacherAssignmentDecorator)comboTeacher3.getSelectedItem();
					if (t1 == null || t1 == noTeacher){
						JOptionPane.showMessageDialog(EditCompanionshipDialog.this,
								"Teacher 1 is an invalid teacher.", "Missing Teacher",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						comp.setTeacher1(t1.getTeacher());
						if (t2 == null || t2 == noTeacher){
							comp.setTeacher2(null);
						} else {
							comp.setTeacher2(t2.getTeacher());
						}
						if (t3 == null || t3 == noTeacher){
							comp.setTeacher3(null);
						} else {
							comp.setTeacher3(t3.getTeacher());
						}
						cancelled = false;
						setVisible(false);
					}
				}
			});
		}
		return btnOk;
	}

	/**
	 * This method initializes btnCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = true;
					setVisible(false);
				}
			});
		}
		return btnCancel;
	}

	/**
	 * This method initializes panelSpacer	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelSpacer() {
		if (panelSpacer == null) {
			lblNote = new JLabel();
			lblNote.setText(" * The teacher is assigned to a companionship.");
			panelSpacer = new JPanel();
			panelSpacer.setLayout(new BorderLayout());
			panelSpacer.add(lblNote, BorderLayout.CENTER);
		}
		return panelSpacer;
	}
	
	/**
	 * @return the cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * @return the teacher
	 */
	public Companionship getCompanionship() {
		return comp;
	}

	/**
	 * This method initializes comboTeacher1	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getComboTeacher1() {
		if (comboTeacher1 == null) {
			comboTeacher1 = new JComboBox();
		}
		return comboTeacher1;
	}

	/**
	 * This method initializes comboTeacher2	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getComboTeacher2() {
		if (comboTeacher2 == null) {
			comboTeacher2 = new JComboBox();
		}
		return comboTeacher2;
	}

	/**
	 * This method initializes comboTeacher3	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getComboTeacher3() {
		if (comboTeacher3 == null) {
			comboTeacher3 = new JComboBox();
		}
		return comboTeacher3;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
