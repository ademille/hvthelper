/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ademille.hvthelper.gui.dialogs;

import javax.swing.JPanel;
import java.awt.Frame;
import java.awt.BorderLayout;
import javax.swing.JDialog;
import java.awt.GridBagLayout;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.GridBagConstraints;
import javax.swing.JButton;

import com.ademille.hvthelper.gui.GroupType;
import com.ademille.hvthelper.gui.util.CenterDialog;

import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.JRadioButton;
import java.awt.Dimension;

public class ReportOptionsDialog extends JDialog {
	
	private boolean cancelled = false;

	private static final long serialVersionUID = 1L;

	private JPanel jContentPane = null;

	private JPanel panelOptions = null;

	private JLabel lblDescription = null;

	private JPanel panelSpacer = null;

	private JPanel panelButtons = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private JRadioButton radioEldersQuorum = null;

	private JRadioButton radioHighPriest = null;

	private JRadioButton radioReliefSociety = null;
	
	private ButtonGroup buttonGroup = null;

	/**
	 * @param owner
	 */
	public ReportOptionsDialog(Frame owner) {
		super(owner);
		initialize();
		CenterDialog.centerDialog(owner, this);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(344, 229);
		this.setSize(new Dimension(344, 186));
		this.setTitle("Report Options");
		this.setContentPane(getJContentPane());
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			BorderLayout borderLayout = new BorderLayout();
			borderLayout.setHgap(5);
			borderLayout.setVgap(5);
			lblDescription = new JLabel();
			lblDescription.setText("Select the groups to include in the report.");
			lblDescription.setHorizontalAlignment(SwingConstants.CENTER);
			lblDescription.setFont(new Font("Dialog", Font.BOLD, 14));
			lblDescription.setHorizontalTextPosition(SwingConstants.CENTER);
			jContentPane = new JPanel();
			jContentPane.setLayout(borderLayout);
			jContentPane.add(getPanelOptions(), BorderLayout.CENTER);
			jContentPane.add(lblDescription, BorderLayout.NORTH);
			jContentPane.add(getPanelButtons(), BorderLayout.SOUTH);
			
			buttonGroup = new ButtonGroup();
			buttonGroup.add(radioEldersQuorum);
			buttonGroup.add(radioHighPriest);
			buttonGroup.add(radioReliefSociety);
		}
		return jContentPane;
	}

	/**
	 * This method initializes panelOptions	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelOptions() {
		if (panelOptions == null) {
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			gridBagConstraints4.gridx = 0;
			gridBagConstraints4.anchor = GridBagConstraints.WEST;
			gridBagConstraints4.gridy = 2;
			GridBagConstraints gridBagConstraints21 = new GridBagConstraints();
			gridBagConstraints21.gridx = 0;
			gridBagConstraints21.anchor = GridBagConstraints.WEST;
			gridBagConstraints21.gridy = 1;
			GridBagConstraints gridBagConstraints12 = new GridBagConstraints();
			gridBagConstraints12.gridx = 0;
			gridBagConstraints12.anchor = GridBagConstraints.WEST;
			gridBagConstraints12.gridy = 0;
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 0;
			gridBagConstraints11.weighty = 2.0D;
			gridBagConstraints11.weightx = 0.0D;
			gridBagConstraints11.gridy = 4;
			panelOptions = new JPanel();
			panelOptions.setLayout(new GridBagLayout());
			panelOptions.add(getPanelSpacer(), gridBagConstraints11);
			panelOptions.add(getRadioEldersQuorum(), gridBagConstraints12);
			panelOptions.add(getRadioHighPriest(), gridBagConstraints21);
			panelOptions.add(getRadioReliefSociety(), gridBagConstraints4);
		}
		return panelOptions;
	}

	/**
	 * This method initializes panelSpacer	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelSpacer() {
		if (panelSpacer == null) {
			panelSpacer = new JPanel();
			panelSpacer.setLayout(new GridBagLayout());
		}
		return panelSpacer;
	}

	/**
	 * This method initializes panelButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setHgap(15);
			panelButtons = new JPanel();
			panelButtons.setLayout(flowLayout);
			panelButtons.add(getBtnOk(), null);
			panelButtons.add(getBtnCancel(), null);
		}
		return panelButtons;
	}

	/**
	 * This method initializes btnOk	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("Ok");
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = false;
					setVisible(false);
				}
			});
		}
		return btnOk;
	}

	/**
	 * This method initializes btnCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = true;
					setVisible(false);
				}
			});
		}
		return btnCancel;
	}
	
	public GroupType getGroupType(){
		if (radioEldersQuorum.isSelected()){
			return GroupType.ELDERS;
		} else if (radioHighPriest.isSelected()){
			return GroupType.HIGH_PRIESTS;
		} else {
			return GroupType.RELIEF_SOCIETY;
		}
			
	}
	
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * This method initializes radioEldersQuorum	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRadioEldersQuorum() {
		if (radioEldersQuorum == null) {
			radioEldersQuorum = new JRadioButton();
			radioEldersQuorum.setText("Elders Quorum");
			radioEldersQuorum.setSelected(true);
		}
		return radioEldersQuorum;
	}

	/**
	 * This method initializes radioHighPriest	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRadioHighPriest() {
		if (radioHighPriest == null) {
			radioHighPriest = new JRadioButton();
			radioHighPriest.setText("High Priests");
		}
		return radioHighPriest;
	}

	/**
	 * This method initializes radioReliefSociety	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRadioReliefSociety() {
		if (radioReliefSociety == null) {
			radioReliefSociety = new JRadioButton();
			radioReliefSociety.setText("Relief Society");
		}
		return radioReliefSociety;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
