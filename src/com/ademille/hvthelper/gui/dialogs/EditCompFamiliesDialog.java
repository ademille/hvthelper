package com.ademille.hvthelper.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ademille.hvthelper.gui.util.CenterDialog;
import com.ademille.hvthelper.gui.util.SortedListModel;
import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;
import javax.swing.JList;
import javax.swing.BorderFactory;
import javax.swing.border.BevelBorder;
import java.awt.Insets;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class EditCompFamiliesDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel content = null;

	private JPanel panelButtons = null;

	private JButton btnOk = null;

	private JButton btnCancel = null;

	private Companionship comp= null;  //  @jve:decl-index=0:
	private boolean cancelled = true;

	private JPanel panelCompInfo = null;

	private JPanel panelFamilySelect = null;

	private JList lstUnassigned = null;

	private JList lstAssigned = null;

	private JPanel panelMoveButtons = null;

	private JButton btnAssign = null;

	private JButton btnUnassign = null;

	private JPanel panelUnassigned = null;

	private JPanel panelAssigned = null;

	private JScrollPane scrollPaneUnassigned = null;

	private JScrollPane scrollPaneAssigned = null;

	private JLabel lblUnassigned = null;

	private JLabel lblAssigned = null;
	
	private DefaultListModel assignedModel;
	private DefaultListModel unassignedModel;
	
	private List<Family> addedFamilies; //assigned families that weren't before.
	private List<Family> removedFamiles; //Families the were unassigned.
	

	public EditCompFamiliesDialog(Frame owner, Companionship comp, TeachingAssignmentInfo info){
		super(owner);
	// A teacher object was provided, so we will edit it.
		this.comp = comp;
		
		initialize();
		setTitle(comp.toString());
		CenterDialog.centerDialog(owner, this);
		
		unassignedModel = new DefaultListModel();		
		for (Family f:info.getUnassigned()){
			unassignedModel.addElement(f);
		}
		lstUnassigned.setModel(new SortedListModel(unassignedModel));
		
		assignedModel = new DefaultListModel();
		for (Family f: comp.getFamilies()){
			assignedModel.addElement(f);
		}
		
		lstAssigned.setModel(new SortedListModel(assignedModel));
		
		addedFamilies = new LinkedList<Family>();
		removedFamiles = new LinkedList<Family>();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(532, 419);
		this.setContentPane(getContent());
	}
	
	/**
	 * This method initializes content
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getContent() {
		if (content == null) {
			content = new JPanel();
			content.setLayout(new BorderLayout());
			content.add(getPanelButtons(), BorderLayout.SOUTH);
			content.add(getPanelCompInfo(), BorderLayout.NORTH);
			content.add(getPanelFamilySelect(), BorderLayout.CENTER);
		}
		return content;
	}

	/**
	 * This method initializes panelButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelButtons() {
		if (panelButtons == null) {
			FlowLayout flowLayout = new FlowLayout();
			flowLayout.setHgap(15);
			panelButtons = new JPanel();
			panelButtons.setLayout(flowLayout);
			panelButtons.add(getBtnOk());
			panelButtons.add(getBtnCancel());
		}
		return panelButtons;
	}

	/**
	 * This method initializes btnOk	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("OK");
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
				cancelled = false;
				setVisible(false);
				}
			});
		}
		return btnOk;
	}

	/**
	 * This method initializes btnCancel	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cancelled = true;
					setVisible(false);
				}
			});
		}
		return btnCancel;
	}

	/**
	 * @return the cancelled
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * @return the teacher
	 */
	public Companionship getCompanionship() {
		return comp;
	}

	/**
	 * This method initializes panelCompInfo	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelCompInfo() {
		if (panelCompInfo == null) {
			panelCompInfo = new JPanel();
			panelCompInfo.setLayout(new GridBagLayout());
		}
		return panelCompInfo;
	}

	/**
	 * This method initializes panelFamilySelect	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelFamilySelect() {
		if (panelFamilySelect == null) {
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.gridx = 2;
			gridBagConstraints3.fill = GridBagConstraints.BOTH;
			gridBagConstraints3.weightx = 1.0D;
			gridBagConstraints3.weighty = 1.0D;
			gridBagConstraints3.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints3.gridy = 0;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.weightx = 1.0D;
			gridBagConstraints2.weighty = 1.0D;
			gridBagConstraints2.fill = GridBagConstraints.BOTH;
			gridBagConstraints2.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints2.gridy = 0;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 1;
			gridBagConstraints1.gridy = 0;
			panelFamilySelect = new JPanel();
			panelFamilySelect.setLayout(new GridBagLayout());
			panelFamilySelect.add(getPanelMoveButtons(), gridBagConstraints1);
			panelFamilySelect.add(getPanelUnassigned(), gridBagConstraints2);
			panelFamilySelect.add(getPanelAssigned(), gridBagConstraints3);
		}
		return panelFamilySelect;
	}

	/**
	 * This method initializes lstUnassigned	
	 * 	
	 * @return javax.swing.JList	
	 */
	private JList getLstUnassigned() {
		if (lstUnassigned == null) {
			lstUnassigned = new JList();
		}
		return lstUnassigned;
	}

	/**
	 * This method initializes lstAssigned	
	 * 	
	 * @return javax.swing.JList	
	 */
	private JList getLstAssigned() {
		if (lstAssigned == null) {
			lstAssigned = new JList();
		}
		return lstAssigned;
	}

	/**
	 * This method initializes panelMoveButtons	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelMoveButtons() {
		if (panelMoveButtons == null) {
			GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
			gridBagConstraints11.gridx = 0;
			gridBagConstraints11.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints11.gridy = 0;
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.insets = new Insets(5, 5, 5, 5);
			gridBagConstraints.gridy = 1;
			panelMoveButtons = new JPanel();
			panelMoveButtons.setLayout(new GridBagLayout());
			panelMoveButtons.add(getBtnAssign(), gridBagConstraints11);
			panelMoveButtons.add(getBtnUnassign(), gridBagConstraints);
		}
		return panelMoveButtons;
	}

	/**
	 * This method initializes btnAssign	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnAssign() {
		if (btnAssign == null) {
			btnAssign = new JButton();
			btnAssign.setText("");
			btnAssign.setIcon(new ImageIcon(getClass().getResource("/com/ademille/hvthelper/gui/icons/forward.png")));
			btnAssign.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Object[] fams = lstUnassigned.getSelectedValues();
					int[] destIndices = new int[fams.length];
					int pos = 0;
					for (Object o: fams){
						Family f = (Family)o;
						assignedModel.addElement(f);
						unassignedModel.removeElement(f);
						//Only add the family, if it isn't already assigned to the companionship.
						if (!comp.getFamilies().contains(f)){
							addedFamilies.add(f);
						}
						//No longer remove the family if we were going to.
						removedFamiles.remove(f);
						lstAssigned.setSelectedValue(f,false);
						destIndices[pos++] = lstAssigned.getSelectedIndex();
					}
					//Highlight the moved families.
					lstAssigned.setSelectedIndices(destIndices);
				}
			});
		}
		return btnAssign;
	}

	/**
	 * This method initializes btnUnassign	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtnUnassign() {
		if (btnUnassign == null) {
			btnUnassign = new JButton();
			btnUnassign.setText("");
			btnUnassign.setIcon(new ImageIcon(getClass().getResource("/com/ademille/hvthelper/gui/icons/back.png")));
			btnUnassign.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Object[] fams = lstAssigned.getSelectedValues();
					int[] destIndices = new int[fams.length];
					int pos = 0;
					for (Object o:fams){
						Family f = (Family)o;
						if (addedFamilies.contains(f)){
							//The family was never officially assigned so just remove from the addedFamiles and model.
							addedFamilies.remove(f);
						} else {
							//This Family was assigned when the dialog was opened, so, add to the removed family list.
							removedFamiles.add(f);
						}
						//Always move from the assignedModel to the unassignedModel
						assignedModel.removeElement(f);
						unassignedModel.addElement(f);
						lstUnassigned.setSelectedValue(f, false);
						destIndices[pos++] = lstUnassigned.getSelectedIndex();
					}
					//Highlight the moved families.
					lstUnassigned.setSelectedIndices(destIndices);
				}
			});
		}
		return btnUnassign;
	}

	/**
	 * This method initializes panelUnassigned	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelUnassigned() {
		if (panelUnassigned == null) {
			lblUnassigned = new JLabel();
			lblUnassigned.setText("Unassigned Families");
			lblUnassigned.setHorizontalAlignment(SwingConstants.CENTER);
			panelUnassigned = new JPanel();
			panelUnassigned.setLayout(new BorderLayout());
			panelUnassigned.setPreferredSize(new Dimension(200, 135));
			panelUnassigned.add(getScrollPaneUnassigned(), BorderLayout.CENTER);
			panelUnassigned.add(lblUnassigned, BorderLayout.NORTH);
		}
		return panelUnassigned;
	}

	/**
	 * This method initializes panelAssigned	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getPanelAssigned() {
		if (panelAssigned == null) {
			lblAssigned = new JLabel();
			lblAssigned.setText("Assigned to this Companionship");
			lblAssigned.setHorizontalAlignment(SwingConstants.CENTER);
			panelAssigned = new JPanel();
			panelAssigned.setLayout(new BorderLayout());
			panelAssigned.setPreferredSize(new Dimension(200, 135));
			panelAssigned.add(getScrollPaneAssigned(), BorderLayout.CENTER);
			panelAssigned.add(lblAssigned, BorderLayout.NORTH);
		}
		return panelAssigned;
	}

	/**
	 * This method initializes scrollPaneUnassigned	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getScrollPaneUnassigned() {
		if (scrollPaneUnassigned == null) {
			scrollPaneUnassigned = new JScrollPane();
			scrollPaneUnassigned.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
			scrollPaneUnassigned.setViewportView(getLstUnassigned());
		}
		return scrollPaneUnassigned;
	}

	/**
	 * This method initializes scrollPaneAssigned	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getScrollPaneAssigned() {
		if (scrollPaneAssigned == null) {
			scrollPaneAssigned = new JScrollPane();
			scrollPaneAssigned.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
			scrollPaneAssigned.setViewportView(getLstAssigned());
		}
		return scrollPaneAssigned;
	}

	/**
	 * @return the addedFamilies
	 */
	public List<Family> getAddedFamilies() {
		return addedFamilies;
	}

	/**
	 * @return the removedFamiles
	 */
	public List<Family> getRemovedFamiles() {
		return removedFamiles;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
