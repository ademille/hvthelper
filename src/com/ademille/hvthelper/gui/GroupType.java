package com.ademille.hvthelper.gui;

public enum GroupType {
  ELDERS, HIGH_PRIESTS, RELIEF_SOCIETY
}
