package com.ademille.hvthelper.gui.util;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Window;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

public class ImageUtil {
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper"); 
	
	//Create a map so that if the same image is asked for twice, it will only be created once.
  private static Map<String, ImageIcon> iconMap = new HashMap<String, ImageIcon>();
	
	 /** Returns an ImageIcon, or null if the path was invalid. */
  public static ImageIcon createIcon(String imageName) {
  	ImageIcon icon = null;
  	if ((icon = iconMap.get(imageName)) == null) {
			// The image can't be found so create a new one.
			String imgLocation = "/com/ademille/hvthelper/gui/icons/" + imageName;
			URL imageURL = ImageUtil.class.getResource(imgLocation);
			if (imageURL == null) {
				logger.error("Resource not found: " + imgLocation);
				icon = null;
			} else {
				//Create and save the new icon.
				icon = new ImageIcon(imageURL);
				iconMap.put(imageName,icon);
			}
  	} 
  	return icon;    
  }
  
  public static Point calculateCenter(Container popupFrame) {  
     KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();   
     Window windowFrame = kfm.getFocusedWindow();  
      Point frameTopLeft = windowFrame.getLocation();  
     Dimension frameSize = windowFrame.getSize();  
     Dimension popSize = popupFrame.getSize();  
      
      int x = (int)(frameTopLeft.getX() + (frameSize.width/2) - (popSize.width/2));  
      int y = (int)(frameTopLeft.getY() + (frameSize.height/2) - (popSize.height/2));  
      Point center = new Point(x, y);  
      return center;  
    }  

}
