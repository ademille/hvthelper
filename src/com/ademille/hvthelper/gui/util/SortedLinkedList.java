/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ademille.hvthelper.gui.util;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * SortedLinkList is a Linked List that is always sorted.  Each element that is inserted
 * will cause the list to be resorted.
 * @author ademille
 *
 * @param <E>
 */
public class SortedLinkedList<E extends Comparable<? super E>> extends LinkedList<E> implements List<E> {
	public SortedLinkedList() {
		super();
	}
	
	public SortedLinkedList(Collection<? extends E> c) {
	 super(c);
	}
	
	@Override
	public boolean add(E e) 
	{
		boolean retVal = super.add(e);
		Collections.sort(this);
		return retVal;
	};
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean retVal = super.addAll(c);
		Collections.sort(this);
		return retVal;
	}
	
	@Override
	public void add(int index, E element) 
	{
		super.add(index,element);
		Collections.sort(this);
	};
	
	@Override
	public void addFirst(E e) 
	{
		super.addFirst(e);
		Collections.sort(this);
	};
	
	@Override
	public void addLast(E e) 
	{
		super.addLast(e);
		Collections.sort(this);
	};

}
