package com.ademille.hvthelper.gui.util;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CompanionVisitTable extends JTable {
public CompanionVisitTable() {
	super();
	
}
@Override
	public Component prepareRenderer(TableCellRenderer renderer, int row,
			int column) {
		Component comp = super.prepareRenderer(renderer, row, column);
		// even index, selected or not selected
		if (row % 2 == 0 && !isCellSelected(row, column)) {
			comp.setBackground(Color.lightGray);
		} else {
			comp.setBackground(Color.white);
		}
		return comp;
	}

}