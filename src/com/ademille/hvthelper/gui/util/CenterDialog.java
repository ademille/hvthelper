package com.ademille.hvthelper.gui.util;

import java.awt.Frame;
import java.awt.Rectangle;

import javax.swing.JDialog;

public class CenterDialog {
	
	public static void centerDialog(Frame parent, JDialog dialog){
	     Rectangle r = parent.getBounds();
	     int x = r.x + (r.width - dialog.getSize().width)/2;
	     int y = r.y + (r.height - dialog.getSize().height)/2;
	     dialog.setLocation(x, y);
		 }
}
