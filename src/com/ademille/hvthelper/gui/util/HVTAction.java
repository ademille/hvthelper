package com.ademille.hvthelper.gui.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;

public class HVTAction extends AbstractAction {
	private ActionListener listener;
	public HVTAction(String text, ImageIcon icon, String desc, Integer mnemonic, ActionListener listener) {
		super(text,icon);
		putValue(SHORT_DESCRIPTION, desc);
		putValue(MNEMONIC_KEY, mnemonic);
		this.listener = listener;
	}
	
	public HVTAction(String text, ImageIcon icon,  ActionListener listener) {
		super(text,icon);
		putValue(SHORT_DESCRIPTION, text);
		this.listener = listener;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		listener.actionPerformed(e);
	}
}

