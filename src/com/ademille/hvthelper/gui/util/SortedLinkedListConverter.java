/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.util;

import com.thoughtworks.xstream.converters.collections.CollectionConverter;
import com.thoughtworks.xstream.mapper.Mapper;

/**
 * This class allows SortedLinkedList objects to be serialized.
 * Since a SortedLinkedList is really just a linked list, I'll simply
 * overwrite the canConvert method to say that SortedLinkedList is good
 * and use all the default implementations of CollectionConverter.
 * @author ademille
 *
 */
public class SortedLinkedListConverter extends CollectionConverter {

	public SortedLinkedListConverter(Mapper mapper) {
		super(mapper);
	}
	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class type) {
		 return type.equals(SortedLinkedList.class)
		 || super.canConvert(type);
 }

}
