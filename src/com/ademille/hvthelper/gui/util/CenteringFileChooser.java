package com.ademille.hvthelper.gui.util;

import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.Point;

import javax.swing.JDialog;
import javax.swing.JFileChooser;


public class CenteringFileChooser extends JFileChooser{

	@Override
	protected JDialog createDialog(Component parent) throws HeadlessException {
		JDialog dialog = super.createDialog(parent);
		Point p = ImageUtil.calculateCenter(dialog);
		dialog.setLocation(p);
		return dialog;
	}

}
