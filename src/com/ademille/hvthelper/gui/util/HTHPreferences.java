/*
 * Copyright (C) 2011  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.gui.util;

import java.util.ArrayList;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;

/**
 * HTHPreferences. This class provides a way to save and restore user-defined
 * preferences.
 * 
 * @author Aaron DeMille
 * 
 */
public class HTHPreferences {
	
	private static final Logger logger = Logger
	.getLogger("com.ademille.hvthelper");
	
	private Preferences prefs = null;
	
	private final String LAST_FILE_PATH = "lastPath";
	private final String LAST_IMPORT_PATH = "lastImportPath";
	private final String MRU_FILE = "mostRecentFile";
	private final int MAX_NUM_MRU_FILES = 5; //Maximum number of most recently used files.
	private ArrayList<String> mruList = null;
	
	public HTHPreferences() {
		prefs = Preferences.userNodeForPackage(getClass());
		mruList = new ArrayList<String>();
		//Build up the MRU list so that we can pop values off the end as new
		//ones are added.
		for (int i = 0; i < MAX_NUM_MRU_FILES; i++){
			String fileName = prefs.get(MRU_FILE + i, null);
			if (fileName != null){
				mruList.add(fileName);
			}
		}
	}
	
	public String getLastFilePath() {
		return prefs.get(LAST_FILE_PATH, "");
	}
	
	public void setLastFilePath(String lastPath) {
		prefs.put(LAST_FILE_PATH, lastPath);
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			logger.error("Unable to save preferences:" + e );
		}
	}
	
	public String getLastImportPath() {
		return prefs.get(LAST_IMPORT_PATH, "");
	}
	
	public void setLastImportPath(String lastPath) {
		prefs.put(LAST_IMPORT_PATH, lastPath);
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			logger.error("Unable to save preferences:" + e );
		}
	}
	
	public void addRecentFile(String fileName){
		//Check if the filename is already an MRU document.
		//If it is promote it to the beginning of the list.
		int index = mruList.indexOf(fileName);
		if (index != -1) {
			//Remove the file from the list.
			mruList.remove(index);
		} 
		
		//If there isn't room remove the oldest file name.
		if (mruList.size() >= MAX_NUM_MRU_FILES){
			mruList.remove(mruList.size()-1);
		}
		
		//Finally add the filename to the top of the list.  
		mruList.add(0, fileName);
		
		//Write out the new preferences.
		for (int i=0; i< mruList.size(); i++){
			String mru = MRU_FILE + i;
			prefs.put(mru, mruList.get(i));
		}
		
		try {
			prefs.flush();
		} catch (BackingStoreException e) {
			logger.error("Unable to save preferences:" + e );
		}
	}
	
	/**
	 * Returns the most recently used document.
	 * @param index The number of the MRU document to retrieve.
	 * @return The MRU document or null if it doesn't exist in the preferences storage.
	 */
	public String getRecentFile(int index){
		String mru = MRU_FILE + index;
		return prefs.get(mru,null);
	}

}
