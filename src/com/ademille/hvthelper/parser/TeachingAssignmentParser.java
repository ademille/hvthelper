/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.parser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ademille.hvthelper.model.Companionship;
import com.ademille.hvthelper.model.District;
import com.ademille.hvthelper.model.Family;
import com.ademille.hvthelper.model.Quorum;
import com.ademille.hvthelper.model.Supervisor;
import com.ademille.hvthelper.model.Teacher;

public class TeachingAssignmentParser implements LineParser{

	
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	private boolean hometeaching = true; //Is this home teaching or visiting teaching.
	////////////////////////////////////////////////////////////////////////
	// Column types.  These apply to both HT and VT files.
	////////////////////////////////////////////////////////////////////////
	public enum Column { 
	QUORUM,
	DISTRICT,
	SUPERVISOR,
	SUPERVISOR_PHONE,
	SUPERVISOR_EMAIL,
	COMP_ID,
	TEACHER_1,
	TEACHER_1_PHONE,
	TEACHER_1_EMAIL,
	TEACHER_2,
	TEACHER_2_PHONE,
	TEACHER_2_EMAIL,
	FAMILY,
	FAMILY_PHONE_1 ,
	FAMILY_PHONE_2 ,
	FAMILY_EMAIL,
	STREET_1,
	STREET_2,
	DP ,
	CITY,
	ZIP ,
	STATE,
	COUNTRY
	}
	
	
	private Map<String, Family> families;
	private Map<String, Teacher> teachers;
	private Map<Integer , Companionship> companionships;
	private Map<String,District > districts;
	private Map<String, Quorum> quorums;
	private List<Family> unassigned;
	private Map<Column, Integer> columnMap;
	
	
	
	public TeachingAssignmentParser(){
		hometeaching = false;
		families = new HashMap<String, Family>();
		teachers = new HashMap<String, Teacher>();
		companionships = new HashMap<Integer, Companionship>();
		districts = new HashMap<String, District>();
		quorums = new HashMap<String, Quorum>();
		unassigned = new LinkedList<Family>();
		columnMap = new HashMap<Column, Integer>();
	}
	
	private void createColumnMapping(String[] columns) {
		//Because the column positions may change with new versions of MLS,
		//we need to create a mapping to avoid parse problems.
		for (int i = 0; i < columns.length; i++){
			String val = columns[i].toLowerCase();
			if (val !=null){
				if (val.equals("quorum")){
					columnMap.put(Column.QUORUM,i);
				} else if (val.equals("ht district")){
					columnMap.put(Column.DISTRICT, i);
				} else if (val.equals("vt district")){
					columnMap.put(Column.DISTRICT, i);
				} else if (val.equals("supervisor")){
					columnMap.put(Column.SUPERVISOR, i);
				} else if (val.equals("supv phone")){
					columnMap.put(Column.SUPERVISOR_PHONE, i);
				}else if (val.equals("supv e-mail")){
					columnMap.put(Column.SUPERVISOR_EMAIL, i);
				}else if (val.equals("comp id")){
					columnMap.put(Column.COMP_ID, i);
				//Home Teacher 1	
				}else if (val.equals("home teacher 1")){
					columnMap.put(Column.TEACHER_1, i);
				}else if (val.equals("ht 1 phone")){
					columnMap.put(Column.TEACHER_1_PHONE, i);
				}else if (val.equals("ht 1 e-mail")){
					columnMap.put(Column.TEACHER_1_EMAIL, i);
					
				//Visiting Teacher 1	
				}else if (val.equals("visiting teacher 1")){
					columnMap.put(Column.TEACHER_1, i);
				}else if (val.equals("vt 1 phone")){
					columnMap.put(Column.TEACHER_1_PHONE, i);
				}else if (val.equals("vt 1 e-mail")){
					columnMap.put(Column.TEACHER_1_EMAIL, i);
					
				//Home Teacher 2	
				}else if (val.equals("home teacher 2")){
					columnMap.put(Column.TEACHER_2, i);
				}else if (val.equals("ht 2 phone")){
					columnMap.put(Column.TEACHER_2_PHONE, i);
				}else if (val.equals("ht 2 e-mail")){
					columnMap.put(Column.TEACHER_2_EMAIL, i);
					
				//Visiting Teacher 2	
				}else if (val.equals("visiting teacher 2")){
					columnMap.put(Column.TEACHER_2, i);
				}else if (val.equals("vt 2 phone")){
					columnMap.put(Column.TEACHER_2_PHONE, i);
				}else if (val.equals("vt 2 e-mail")){
					columnMap.put(Column.TEACHER_2_EMAIL, i);
					
				}else if (val.equals("household")){
					columnMap.put(Column.FAMILY, i);
				}else if (val.equals("sister taught")){
					columnMap.put(Column.FAMILY, i);
					
				}else if (val.equals("household phone")){
					columnMap.put(Column.FAMILY_PHONE_1, i);
				}else if (val.equals("phone")){
					columnMap.put(Column.FAMILY_PHONE_1, i);
				}else if (val.equals("phone 1")){
					columnMap.put(Column.FAMILY_PHONE_1, i);
				}else if (val.equals("phone 2")){
					columnMap.put(Column.FAMILY_PHONE_2, i);
					
				} else if (val.equals("e-mail address")){
					columnMap.put(Column.FAMILY_EMAIL, i);
				}	else if (val.equals("street 1")){
					columnMap.put(Column.STREET_1, i);
				}	else if (val.equals("street 2")){
					columnMap.put(Column.STREET_2, i);
				}	else if (val.equals("d/p")){
					columnMap.put(Column.DP, i);
				}	else if (val.equals("city")){
					columnMap.put(Column.CITY, i);
				}	else if (val.equals("postal")){
					columnMap.put(Column.ZIP, i);
				}	else if (val.equals("state/prov")){
					columnMap.put(Column.STATE, i);
				}	else if (val.equals("country")){
					columnMap.put(Column.COUNTRY, i);
				} else if (!val.trim().equals("")){
					logger.warn("Couldn't find match for:"+ val);
				}
			}
		}
		
	}
	
	private String getVal(Column c, String[] columns){
		String retVal = "";
		Integer colPos = columnMap.get(c);
		//Lookup the index of the desired element.
		if (colPos!=null && columns!=null && colPos < columns.length){
			retVal = columns[colPos];
		}
		return retVal;
	}
	
	public void parseLine(String[] line, boolean headerLine){
		
		if (headerLine){
			//Check to see if this is home teaching or visiting teaching.
			hometeaching = "Quorum".equals(line[0]);
			createColumnMapping(line);
		}
		else {
			
			boolean newComp = false; //Does this line contain a new comp.
			boolean newDistrict = false; //Does this line contain a new district.
			
			//Start with the family and end with the 
			//quorum.  We'll need to build the hierarchy from the leaves.
			Family family;
			if ((family = families.get(getVal(Column.FAMILY, line)))==null){
				family = new Family();
				family.setName(getVal(Column.FAMILY, line));
				family.setPhone1(getVal(Column.FAMILY_PHONE_1, line));
				family.setPhone2(getVal(Column.FAMILY_PHONE_2, line));
				family.setEmail(getVal(Column.FAMILY_EMAIL, line));
				family.setStreet1(getVal(Column.STREET_1, line));
				family.setStreet2(getVal(Column.STREET_2, line));
				family.setDp(getVal(Column.DP, line));
				family.setCity(getVal(Column.CITY, line));
				family.setState(getVal(Column.STATE, line));
				family.setZip(getVal(Column.ZIP, line));
				family.setCountry(getVal(Column.COUNTRY, line));
				families.put(family.getName(), family);
			}
			
			//If the family doesn't have home/visiting teachers, then the Companionship ID will be empty.
			//I'll do an early check here and add them to the list of not-visited.
			if ("".equals(getVal(Column.COMP_ID, line))){
				//The family does not have teachers assigned.
				unassigned.add(family);
				
			} else {
				
				//Create teachers
				Teacher teacher1;
				if ((teacher1= teachers.get(getVal(Column.TEACHER_1, line)))==null){
					teacher1 = new Teacher(
					    getVal(Column.TEACHER_1, line),
					    getVal(Column.TEACHER_1_PHONE, line),
					    getVal(Column.TEACHER_1_EMAIL, line));
					//Don't add empty teachers to the teacher list
					if (!teacher1.getName().trim().equals("")){
						teachers.put(teacher1.getName(), teacher1);
					} else {
						teacher1 = null;
					}
				}

				Teacher teacher2;
				if ((teacher2= teachers.get(getVal(Column.TEACHER_2, line)))==null){
					teacher2 = new Teacher(
					    getVal(Column.TEACHER_2, line),
					    getVal(Column.TEACHER_2_PHONE, line),
					    getVal(Column.TEACHER_2_EMAIL, line));
					//Don't add empty teachers to the teacher list
					if (!teacher2.getName().trim().equals("")){
						teachers.put(teacher2.getName(), teacher2);
					} else {
						teacher2 = null;
					}
				}
				
				//Create Companionship, if it hasn't already been created.
				Companionship companionship;
				int compId = 0;
				String compIdStr = getVal(Column.COMP_ID,line);
				try {
					compId = Integer.parseInt(compIdStr);
				} catch (NumberFormatException nfe){
					logger.error("Error Parsing Companion ID:"+ compIdStr);
					return;
				}
				if ((companionship = companionships.get(compId))==null){
					companionship = new Companionship(teacher1, teacher2);
					
					companionships.put(compId, companionship);
					if (null != teacher1){
						teacher1.addCompanionship(companionship);
					}
					if (null != teacher2){
						teacher2.addCompanionship(companionship);
					}
					newComp = true; //This is a new companionship.
				}
				//Add the family to the companionship
				companionship.addFamily(family);
				//Also let the family know who their teachers are.
				family.setCompanionship(companionship);
				
				//Create a district with Supervisor
				District district ;
				Supervisor supervisor;
				//districtId isn't really unique. It could be duplicated for Elders and High Priests.
				//int districtId = Integer.parseInt(columns[DISTRICT -offset]);
				if ((district = districts.get(getVal(Column.SUPERVISOR, line)))==null){
					supervisor = new Supervisor(
					    getVal(Column.SUPERVISOR, line),
					    getVal(Column.SUPERVISOR_PHONE, line),
					    getVal(Column.SUPERVISOR_EMAIL, line));
					district = new District(supervisor);
					districts.put(supervisor.getName(), district);
					newDistrict = true;
				}
				if (newComp){
					//Add the companionship to a district, but only if it hasn't been added before.
					district.addCompanionship(companionship);
				}
				
				// Quorums only exist for home teaching. But, to make the structure consistent, we'll just create
				//a quorum for the visiting teaching called "Districts".
				// Create a quorum object
				Quorum quorum;
				String quorumName = hometeaching ? getVal(Column.QUORUM, line) : "Districts";
				if ((quorum = quorums.get(quorumName)) == null) {
					quorum = new Quorum(quorumName);
					quorums.put(quorum.getName(), quorum);
				}
				if (newDistrict) {
					// Add district to quorum, but only if it hasn't been added before.
					quorum.addDistrict(district);
				}
			} //End else no compId.
		}
	}
	
	
	public boolean isHometeaching() {
		return hometeaching;
	}

	/**
	 * @return the quorums
	 */
	public List<Quorum> getQuorums() {
		LinkedList<Quorum> list = new LinkedList<Quorum>(quorums.values());
		return list;
	}

	/**
	 * @return the teachers
	 */
	public List<Teacher> getTeachers() {
		LinkedList<Teacher> list = new LinkedList<Teacher>(teachers.values());
		return list;
	}

	/**
	 * @return the unassigned
	 */
	public List<Family> getUnassigned() {
		return unassigned;
	}
	

}
