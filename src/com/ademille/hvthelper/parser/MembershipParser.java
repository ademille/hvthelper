/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.ademille.hvthelper.model.Member;

public class MembershipParser implements LineParser {

	/**
	 * The list of ward members.
	 */
	private List<Member> members;
	
	private int numMembers = 0;
	
	private String[] headerFields = null;
	
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	
	public MembershipParser(){
		members = new ArrayList<Member>();
	}

	/**
	 * @return the members
	 */
	public Collection<Member> getMembers() {
		ArrayList<Member> list = new ArrayList<Member>(members);
		Collections.sort(list);
		return list;
	}

	public void parseLine(String[] columns, boolean headerLine) {
		//If it's the first line same the columns so they can be used for a mapping.
		if (headerLine){
			headerFields = columns;
		} 
		else {
			//Create a new Member object and populate all its data field with the columns
			//of the csv file.
			Member m = new Member();
			for (int i = 0; i < columns.length; i++){
				m.addField(headerFields[i], columns[i]);
			}
			//Do a little sanity checking. Regardless of export settings,
			//the full name should be available.
			String fullName = m.getField(Member.FULL_NAME); 
			if (fullName!=null && !fullName.equals("")){
				//Now add the member to the list of members.
				members.add(m);
			} else {
				logger.error("Member entry #"+numMembers+" doesn't have a Full Name entry. Not adding.");
			}
		}
		numMembers ++;
	}
	
}
