/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.parser;

import java.io.File;

import org.apache.log4j.Logger;

import com.ademille.hvthelper.model.HVTeachingInfo;
import com.ademille.hvthelper.model.TeachingAssignmentInfo;

public class MLSParser {
	private static String HT = "HomeTeaching.csv";
	private static String VT = "VisitingTeaching.csv";
	//private static String MEMBER = "Membership.csv";
	//private static String ORG = "Organization.csv";

	private TeachingAssignmentParser htParser = null;
	private TeachingAssignmentInfo htInfo = null;
	private TeachingAssignmentParser vtParser = null;
	private TeachingAssignmentInfo vtInfo = null;
	//private MembershipParser memberParser     = null;
	//private OrganizationParser orgParser      = null;
	
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	public MLSParser(){
		htParser      = new TeachingAssignmentParser();
		vtParser      = new TeachingAssignmentParser();
		//memberParser  = new MembershipParser();
		//orgParser     = new OrganizationParser();
	}
	
	public void parseFiles(String inputDirectory) throws ParserException{
		//Create files for each possible input file.
		File inputDir= new File(inputDirectory);
		if (!inputDir.exists()){
			throw new ParserException("The directory:"+ inputDirectory + " does not exist.");
		}
		
		if (!inputDir.isDirectory()){
			throw new ParserException("The input directory:"+ inputDirectory + " is not a directory.");
		}
		
		//Now see if the files exist.
		File htFile     = findMatchingFile(inputDir,HT); //check camel case
		
		File vtFile     = findMatchingFile(inputDir,VT); //camel case
		//File memberFile = new File(inputDir,MEMBER);
		//File orgFile    = new File(inputDir,ORG);
		
		if (htFile !=null && parseFile(htFile, htParser, "Home Teaching File")){
			// Create teaching info object, so that the data isn't contained in a
			// parser object.
			htInfo = new TeachingAssignmentInfo();
			htInfo.setHometeaching(htParser.isHometeaching());
			htInfo.setQuorums(htParser.getQuorums());
			htInfo.setTeachers(htParser.getTeachers());
			htInfo.setUnassigned(htParser.getUnassigned());
		} else {
			htParser = null;
			htInfo = null;
		}
		
		if (vtFile != null && parseFile(vtFile, vtParser,"Visting Teaching File")){ 
			//Create teaching info object, so that the data isn't contained in a parser object.
			vtInfo = new TeachingAssignmentInfo();
			vtInfo.setHometeaching(vtParser.isHometeaching());
			vtInfo.setQuorums(vtParser.getQuorums());
			vtInfo.setTeachers(vtParser.getTeachers());
			vtInfo.setUnassigned(vtParser.getUnassigned());
		} else {
			vtParser = null;
			vtInfo = null;
		}
		
		/*if (!parseFile(memberFile, memberParser, "Membership File")){
			memberParser = null;
		}
		
		if (!parseFile(orgFile, orgParser, "Organization File")){
			orgParser = null;
		}
		*/
	}
	
	/*
	 * Find a file in the specified directory that matches the the filename.
	 * But, look for the file being case-insensitive.
	 */
	private File findMatchingFile(File  inputDir, String fileName){
		File file = null;
		String lowerFileName = fileName.toLowerCase();
		logger.info("Searching for " + fileName+".");
		if (inputDir.isDirectory()){
			for (File f:inputDir.listFiles()){
				if (f.getName().toLowerCase().equals(lowerFileName)){
					//Found a file that matches!
					logger.info("Found " + f.getName()+".");
					file = f;
				}
			}
		}
		if (file == null){
			logger.warn("Unable to find file: " + fileName+".");
		}
		return file;
	}
	
	
	private boolean parseFile(File file, LineParser lp, String desc) {
		boolean parsed = false;
		logger.info("Parsing " + desc +".");
		if (file.exists()){
			logger.info(file.getAbsolutePath()+ " found.");
			//Parse the File.
			FileParser fp = new FileParser(lp);
			if (fp.parseFile(file)){
				logger.info(file.getAbsolutePath()+ "-  Successfully parsed.");
				parsed = true;
			}
		} else {
			logger.warn(file.getAbsolutePath()+ "- NOT found.  Skipping.");
		}
		return parsed;
	}

	/**
	 * @return the htParser
	 */
	public TeachingAssignmentInfo getHtInfo() {
		return htInfo;
	}

	/**
	 * @return the vtParser
	 */
	public TeachingAssignmentInfo getVtInfo() {
		return vtInfo;
	}
	
	public HVTeachingInfo getHVTeachingInfo() {
		return new HVTeachingInfo(htInfo, vtInfo);
	}


}
