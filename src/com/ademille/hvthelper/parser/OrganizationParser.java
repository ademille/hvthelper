/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ademille.hvthelper.model.Organization;
import com.ademille.hvthelper.model.Position;

public class OrganizationParser implements LineParser{

	
	
	private Map<String, Organization> orgs;
	private List<Position> positions;
	
	////////////////////////////////////////////////////////////////////////
	// Column positions
	////////////////////////////////////////////////////////////////////////
	private static final int ORG_SEQ = 0;
	private static final int ORGANIZATION = 1;
	private static final int POS_SEQ = 2;
	private static final int POSITION = 3;
	private static final int LEADERSHIP = 4;
	private static final int INDIVIDUAL_ID = 5;
	private static final int INDIVIDUAL_NAME = 6;
	private static final int SUSTAINED = 7;
	
	public OrganizationParser(){
		orgs = new HashMap<String, Organization>();
		positions = new ArrayList<Position>();
	}
	
	public void parseLine(String[] columns, boolean headerLine){
		
		if (headerLine){
			//Just skip header line for now. I could do some validation here on the columns.
		}
		else {
			//Setup column offset
			//Start with the organization since positions will need to be added to the org. 
			Organization org;
			if ((org= orgs.get(columns[ORGANIZATION]))==null){
				int orgSeq = Integer.parseInt(columns[ORG_SEQ]);
				org = new Organization(orgSeq,columns[ORGANIZATION]);
				orgs.put(org.getName(), org);
			}
			
			//Create the position and add it to the organization.
			Position position = new Position(org);
			position.setPosSeq(Integer.parseInt(columns[POS_SEQ]));
			position.setName(columns[POSITION]);
			String leadership = columns[LEADERSHIP].toLowerCase();
			position.setLeadership(leadership.equals("yes"));
			String indivId = columns[INDIVIDUAL_ID];
			if (!indivId.equals("")){
				position.setIndividualId(Integer.parseInt(columns[INDIVIDUAL_ID]));
			} 
			position.setIndividualName(columns[INDIVIDUAL_NAME]);
			position.setSustainedDate(columns[SUSTAINED]);
			org.addPosition(position);
			positions.add(position);
		}
	}
	

	/**
	 * @return the organizations
	 */
	public Collection<Organization> getOrganization() {
		ArrayList<Organization> list = new ArrayList<Organization>(orgs.values());
		Collections.sort(list);
		return list;
	}

	/**
	 * @return the positions
	 */
	public List<Position> getPositions() {
		return positions;
	}
	

}
