/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import au.com.bytecode.opencsv.CSVReader;


public class FileParser {

	private LineParser lineParser;
	private static final Logger logger= Logger.getLogger("com.ademille.hvthelper");
	
	
	public FileParser(LineParser lineParser){
		this.lineParser = lineParser;
	}
	
	public boolean parseFile(File file){
		boolean parsed = false;
		try {
			CSVReader reader = new CSVReader(new FileReader(file));
			String[] nextLine;
			boolean headerLine = true;
			while((nextLine = reader.readNext())!=null){
				lineParser.parseLine(nextLine, headerLine);
				if (headerLine){
					headerLine = false;
				} 
			}
			reader.close();
			parsed = true;
		} catch (FileNotFoundException fne){
			logger.warn("Unable to open file:"+ file.getAbsolutePath());
		} catch (IOException e) {
			logger.warn("Error reading file:"+ file.getAbsolutePath());
			logger.warn(e);
		} catch (Exception e){
			logger.error("Error parsing file:"+ file.getAbsolutePath());
			logger.error(e);
		}
		return parsed;
	}
	
}
