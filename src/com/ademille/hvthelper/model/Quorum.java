/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.model;

import java.util.List;

import com.ademille.hvthelper.gui.util.SortedLinkedList;
import com.ademille.hvthelper.visitor.Visitable;
import com.ademille.hvthelper.visitor.Visitor;

public class Quorum implements Visitable, Comparable<Quorum>{
	private String name;
	private List<District> districts;
	
	public Quorum(){
		this("Unknown");
	}
	public Quorum(String name){
		this.name = name;
		districts = new SortedLinkedList<District>();
	}
	
	/**
	 * Create a method that helps in de-serialization. Xstream may de-serialize
	 * older file versions and create a LinkedList instead of a SortedLinkedList
	 * since it doesn't use the constructor. So, force it to be a SortedLinkedList
	 * here.
	 * @return This object.
	 */
	private Object readResolve() {
		districts = new SortedLinkedList<District>(districts);
		return this;
	}
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	
	public void addDistrict(District district){
		districts.add(district);
	}
	
	public List<District> getDistricts(){
		return districts;
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
		for (District d:districts){
			d.accept(visitor);
		}
		
	}
	
	public int compareTo(Quorum q) {
		return this.getName().compareTo(q.getName());
	}

}
