/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.model;

import java.util.ArrayList;
import java.util.List;

public class Teacher implements Comparable<Teacher> {
	private String name;
	private String phone;
	private String email;
	
	private transient static int idCounter = 0;
	private transient int id;
	private List<Companionship> companionships;
	@SuppressWarnings("unused")
	@Deprecated
	private Companionship companionship; //Just here for serialized file compatibility.
	public Teacher(String name, String phone, String email){
		this.companionships = new ArrayList<Companionship>();
		this.name = name;
		this.phone = phone;
		this.email = email;
		assignId();
		
	}
	
	public String toString(){
			return getName();
	}
	
	private void assignId(){
		//Assign a unique ID.
		this.id = ++ idCounter;
	}
	
	/**
	 * Create a method that helps in de-serialization, since the default
	 * constructor isn't called. 
	 * @return This object.
	 */
	private Object readResolve() {
		if (companionships==null){
			this.companionships = new ArrayList<Companionship>();
		}
		assignId();
		return this;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	 public void setEmail(String email) {
	    this.email = email;
	  }

	  public String getEmail() {
	    return email;
	  }
	
	public int getId(){
		return id;
	}
	/**
	 * @return the companionship list
	 */
	public List<Companionship> getCompanionships() {
		return companionships;
	}

	public void addCompanionship(Companionship comp){
		if (!companionships.contains(comp)){
			companionships.add(comp);
		}
	}
	
	public void removeCompanionship(Companionship comp){
		companionships.remove(comp);
	}
	public int compareTo(Teacher t) {
		return this.getName().compareTo(t.getName());
	}
}
