/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package com.ademille.hvthelper.model;

import java.util.LinkedList;
import java.util.List;

import com.ademille.hvthelper.visitor.Visitable;
import com.ademille.hvthelper.visitor.Visitor;

public class Family implements Visitable, Comparable<Family> {
	private String name;
	private String phone1;
	private String phone2;
	private String email;
	private String street1;
	private String street2;
	private String dp;
	private String city;
	private String zip;
	private String state;
	private String country;
	private Companionship companionship;
	private static int idCounter = 0;
	private transient int id;
	/** The list of visits for this family. */
	private List<Visit> visits; 
	
	public Family(){
		//Create a unique id for this family
		//and increment the static family counter.
		assignId();
		this.visits = new LinkedList<Visit>();
	}
	
	private void assignId(){
		//Assign a unique ID.
		this.id = ++ idCounter;
	}
	
	/**
	 * Create a method that helps in de-serialization, since the default
	 * constructor isn't called. 
	 * @return This object.
	 */
	private Object readResolve() {
		assignId();
		return this;
	}
	
	
	public int getId(){
		return id;
	}
	
	public void addVisit(Visit visit){
		this.visits.add(visit);
	}
	
	public void removeVisit(Visit visit){
		this.visits.remove(visit);
	}
	
	/**
	 * @return the companionship
	 */
	public Companionship getCompanionship() {
		return companionship;
	}
	/**
	 * @param companionship the companionship to set
	 */
	public void setCompanionship(Companionship companionship) {
		this.companionship = companionship;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDp() {
		return dp;
	}
	public void setDp(String dp) {
		this.dp = dp;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStreet1() {
		return street1;
	}
	public void setStreet1(String street1) {
		this.street1 = street1;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
	public int compareTo(Family f) {
		return this.getName().compareTo(f.getName());
	}
	
	public String toString(){
		return getName();
	}
	

}
