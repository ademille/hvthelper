/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.model;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.ademille.hvthelper.gui.util.SortedLinkedList;
import com.ademille.hvthelper.visitor.Visitable;
import com.ademille.hvthelper.visitor.Visitor;

public class Companionship implements Visitable, Comparable<Companionship> {
	private transient int id;
	private static int idCounter=0;
	
	/**
	 * Teachers that are part of the companionship.
	 * It might make more sense to just have a list of teachers. That could make some processing
	 * tasks easier.  But, really only 3 or less teachers makes sense.
	 */
	private Teacher teacher1 = null;

	private Teacher teacher2 = null;;
	
	private Teacher teacher3 = null;

	List<Family> families;
	

	public Companionship() {
		families = new SortedLinkedList<Family>();
		assignId();
		
	}
	
	private void assignId(){
		//Assign a unique ID.
		this.id = ++ idCounter;
	}

	/**
	 * Create a method that helps in de-serialization, since the default
	 * constructor isn't called. Also, Xstream may de-serialize older file
	 * versions and create a LinkedList instead of a
	 * SortedLinkedList. So, we'll force it to be a SortedLinkedList here.
	 * 
	 * @return This object.
	 */
	private Object readResolve() {
		assignId();
		families = new SortedLinkedList<Family>(families);
		return this;
	}

	public Companionship(Teacher teacher1, Teacher teacher2) {
		this();
		this.teacher1 = teacher1;
		this.teacher2 = teacher2;
	}
	
	public Teacher getTeacher1() {
		return teacher1;
	}

	public void setTeacher1(Teacher newTeacher) {
    //If we are replacing a teacher.  We need to remove the link to this companionship.
		if (teacher1 !=null){
			teacher1.removeCompanionship(this);
		}
		//Associate the new teacher with this companionship.
		if (newTeacher != null){
			newTeacher.addCompanionship(this);
		}
		teacher1 = newTeacher;
	}

	public Teacher getTeacher2() {
		return teacher2;
	}

	public void setTeacher2(Teacher newTeacher) {
	//If we are replacing a teacher.  We need to remove the link to this companionship.
    if (teacher2 !=null){
        teacher2.removeCompanionship(this);
    }
    //Associate the new teacher with this companionship.
    if (newTeacher != null){
        newTeacher.addCompanionship(this);
    }
    teacher2 = newTeacher;
	}
	
	public Teacher getTeacher3() {
		return teacher3;
	}

	public void setTeacher3(Teacher newTeacher) {
	//If we are replacing a teacher.  We need to remove the link to this companionship.
    if (teacher3 !=null){
        teacher3.removeCompanionship(this);
    }
    //Associate the new teacher with this companionship.
    if (newTeacher != null){
        newTeacher.addCompanionship(this);
    }
    teacher3 = newTeacher;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addFamily(Family f) {
		f.setCompanionship(this);
		families.add(f);
	}

	public void removeFamily(Family f) {
		f.setCompanionship(null);
		families.remove(f);
	}

	public List<Family> getFamilies() {
		return families;
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
		for (Family f : families) {
			f.accept(visitor);
		}
	}
	
	public String joinTeachers(String delimiter){
		List<Teacher> tList = new LinkedList<Teacher>();
		if (teacher1 != null){ tList.add(teacher1);}
		if (teacher2 != null){ tList.add(teacher2);}
		if (teacher3 != null){ tList.add(teacher3);}
		
		StringBuffer buffer = new StringBuffer();
		Iterator<Teacher> iter = tList.iterator();
		if (iter.hasNext()){
			buffer.append(iter.next().getName());
			while (iter.hasNext()){
				buffer.append(delimiter).append(iter.next().getName());
			}
		}
		return buffer.toString();
	}

	public String toString() {
		return joinTeachers(" and ");
	}

	@Override
	public int compareTo(Companionship o) {
		return toString().compareTo(o.toString());
	}

}
