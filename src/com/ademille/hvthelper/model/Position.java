/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.model;

public class Position {
	private Organization org;
	private int posSeq;
	private String name;
	private boolean leadership;
	private Member member;
	private int individualId = 0;
	private String individualName;
	private String sustainedDate;
	
	public Position(Organization org){
		this.org = org;
	}

	/**
	 * @return the individualId
	 */
	public int getIndividualId() {
		return individualId;
	}

	/**
	 * @param individualId the individualId to set
	 */
	public void setIndividualId(int individualId) {
		this.individualId = individualId;
	}

	/**
	 * @return the individualName
	 */
	public String getIndividualName() {
		return individualName;
	}

	/**
	 * @param individualName the individualName to set
	 */
	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}

	/**
	 * @return the leadership
	 */
	public boolean isLeadership() {
		return leadership;
	}

	/**
	 * @param leadership the leadership to set
	 */
	public void setLeadership(boolean leadership) {
		this.leadership = leadership;
	}

	/**
	 * @return the member
	 */
	public Member getMember() {
		return member;
	}

	/**
	 * @param member the member to set
	 */
	public void setMember(Member member) {
		this.member = member;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the org
	 */
	public Organization getOrg() {
		return org;
	}

	/**
	 * @param org the org to set
	 */
	public void setOrg(Organization org) {
		this.org = org;
	}

	/**
	 * @return the posSeq
	 */
	public int getPosSeq() {
		return posSeq;
	}

	/**
	 * @param posSeq the posSeq to set
	 */
	public void setPosSeq(int posSeq) {
		this.posSeq = posSeq;
	}

	/**
	 * @return the sustainedDate
	 */
	public String getSustainedDate() {
		return sustainedDate;
	}

	/**
	 * @param sustainedDate the sustainedDate to set
	 */
	public void setSustainedDate(String sustainedDate) {
		this.sustainedDate = sustainedDate;
	}

}
