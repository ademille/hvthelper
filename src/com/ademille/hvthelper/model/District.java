/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package com.ademille.hvthelper.model;

import java.util.Collection;
import java.util.List;

import com.ademille.hvthelper.gui.util.SortedLinkedList;
import com.ademille.hvthelper.visitor.Visitable;
import com.ademille.hvthelper.visitor.Visitor;

public class District implements Visitable, Comparable<District> {
	private transient int id;
	private static int idCounter = 0; //A counter to make a unique ID if one isn't provided.
	private Supervisor supervisor;
	private List<Companionship> companionships;
	
	
	public District(Supervisor supervisor){
		//Assign a unique Id for each instance.
		assignId();
		this.supervisor = supervisor;
		companionships = new SortedLinkedList<Companionship>();
	}
	
	private void assignId(){
		//Assign a unique ID.
		this.id = ++idCounter;
	}
	
 /* Create a method that helps in de-serialization, since the default
	 * constructor isn't called. Also, Xstream may de-serialize older file
	 * versions and create a LinkedList instead of a
	 * SortedLinkedList. So, we'll force it to be a SortedLinkedList here.
	 */
	private Object readResolve() {
		assignId();
		companionships = new SortedLinkedList<Companionship>(companionships);
		return this;
	}

	public int getId() {
		return id;
	}

	
	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}
	
	public void addCompanionship(Companionship comp){
		companionships.add(comp);
	}
	
	public void removeCompanionship(Companionship comp){
		companionships.remove(comp);
	}

	public void accept(Visitor visitor) {
		visitor.visit(this);
		for (Companionship c:companionships){
			c.accept(visitor);
		}
	}
	
	public Collection<Companionship> getCompanionships(){
		return companionships;
	}
	
	public int compareTo(District d) {
		return this.getSupervisor().getName().compareTo(d.getSupervisor().getName());
		/*if (this.getId() < d.getId()){
			return -1;
		} else if (getId() == d.getId()){
			return 0;
		} else {
			return 1;
		}
		*/
	}
	
}
