/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.model;

import java.util.Collections;
import java.util.List;

import com.ademille.hvthelper.gui.util.SortedLinkedList;

public class TeachingAssignmentInfo {
	private boolean hometeaching = false;

	private List<Teacher> teachers;

	private List<Quorum> quorums;

	private List<Family> unassigned;
	
	public TeachingAssignmentInfo(){
		teachers = new SortedLinkedList<Teacher>();
		quorums = new SortedLinkedList<Quorum>();
		unassigned = new SortedLinkedList<Family>();
	}

	/**
	 * Create a method that helps in de-serialization. Xstream may de-serialize
	 * older file versions and create a LinkedList instead of a SortedLinkedList
	 * since it doesn't use the constructor. So, force it to be a SortedLinkedList
	 * here.
	  * 
	 * @return This object.
	 */
	private Object readResolve() {
		teachers = new SortedLinkedList<Teacher>(teachers);
		quorums = new SortedLinkedList<Quorum>(quorums);
		unassigned = new SortedLinkedList<Family>(unassigned);
		return this;
	}

	/**
	 * @return the hometeaching
	 */
	public boolean isHometeaching() {
		return hometeaching;
	}

	/**
	 * @param hometeaching
	 *          the hometeaching to set
	 */
	public void setHometeaching(boolean hometeaching) {
		this.hometeaching = hometeaching;
	}

	/**
	 * @return the quorums
	 */
	public List<Quorum> getQuorums() {
		return quorums;
	}

	/**
	 * @param quorums
	 *          the quorums to set
	 */
	public void setQuorums(List<Quorum> quorums) {
		//Make sure the quorums are sorted.
		this.quorums = new SortedLinkedList<Quorum>(quorums);
	}

	/**
	 * @return the teachers
	 */
	public List<Teacher> getTeachers() {
		return teachers;
	}

	/**
	 * @param teachers
	 *          the teachers to set
	 */
	public void setTeachers(List<Teacher> teachers) {
		//Make sure the teachers are sorted.
		this.teachers = new SortedLinkedList<Teacher>(teachers);
		
	}
	
	public void addTeacher(Teacher t){
		if (null != teachers){
			teachers.add(t);
			Collections.sort(teachers);
		}
	}
	public void removeTeacher(Teacher t){
		if (null != teachers){
			teachers.remove(t);
		}
	}
	/**
	 * @return the unassigned
	 */
	public List<Family> getUnassigned() {
		return unassigned;
	}

	/**
	 * @param unassigned
	 *          the unassigned to set
	 */
	public void setUnassigned(List<Family> unassigned) {
		this.unassigned = new SortedLinkedList<Family>(unassigned);
	}
	
	public void addUnassigned(Family f){
		if (null != unassigned){
			unassigned.add(f);
		}
	}
	
	public void removeUnassigned(Family f){
		if (null != unassigned){
			unassigned.remove(f);
		}
	}

}
