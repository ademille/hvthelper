/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.model;

import java.util.ArrayList;
import java.util.List;

public class Organization implements Comparable<Organization> {
	private int orgSeq;
	private String name;
	private List<Position> positions;
	
	public Organization(int orgSeq, String name){
		this.orgSeq = orgSeq;
		this.name = name;
		positions = new ArrayList<Position>();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the orgSeq
	 */
	public int getOrgSeq() {
		return orgSeq;
	}

	/**
	 * @param orgSeq the orgSeq to set
	 */
	public void setOrgSeq(int orgSeq) {
		this.orgSeq = orgSeq;
	}

	/**
	 * @return the positions
	 */
	public List<Position> getPositions() {
		return positions;
	}
	
	public void addPosition(Position position){
		positions.add(position);
	}

	public int compareTo(Organization org) {
		return getOrgSeq() - org.getOrgSeq();
	}
	

}
