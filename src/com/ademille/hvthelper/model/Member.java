/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hvthelper.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;


public class Member implements Comparable<Member> {
	  //Create mapping between column names and more code friendly names.
	  //This will also provide a layer of indirection in case the column names change.
		public static final String ID = "Indiv ID";
		public static final String FULL_NAME = "Full Name";
		public static final String PREF_NAME="Preferred Name";
		public static final String MRN = "MRN";
		public static final String HOH_ID = "HofH ID";
		public static final String HH_POS ="HH Position";
		public static final String HH_ORDER ="HH Order";
		public static final String PHONE_1 ="Phone 1";
		public static final String PHONE_2 ="Phone 2";
		public static final String EMAIL ="E-mail Address";
		public static final String STREET_1 ="Street 1";
		public static final String STREET_2 ="Street 2";
		public static final String DP ="D/P";
		public static final String CITY ="City";
		public static final String ZIP ="Postal";
		public static final String STATE ="State/Prov";
		public static final String COUNTRY ="Country";
		public static final String SEX ="Sex";
		public static final String BIRTH ="Birth";
		public static final String BAPTIZED ="Baptized";
		public static final String CONFIRMED ="Confirmed";
		public static final String ENDOWED ="Endowed";
		public static final String REC_EXP ="Rec Exp";
		public static final String PRIESTHOOD ="Priesthood";
		public static final String MISSION ="Mission";
		public static final String MARRIED ="Married";
		public static final String SPOUSE_MEMBER ="Spouse Member";
		public static final String SEALED_TO_SPOUSE ="Sealed to Spouse";
		public static final String SEALED_TO_PRIOR = "Sealed to Prior";
		
		private Map<String,String> fieldMap;
		
		private static final Logger logger= Logger.getLogger(Member.class);
		
		public Member(){
			fieldMap = new HashMap<String, String>();
		}
		
		public void addField(String column, String value){
			fieldMap.put(column, value);
		}
		
		public String getField(String column){
			return fieldMap.get(column); 
		}
		
		public int compareTo(Member f) {
			String name1 = f.getField(FULL_NAME);
			String name2 = this.getField(FULL_NAME);
			if (name1 != null && name2 != null){
				return name2.compareTo(name1);
			}
			else{
				//For some reason there isn't a full name field. So, just say they are equal.
				//This should really never happen if the file is good.
				logger.error("A member object doesn't have a Full Name entry. This should never happen if the file is good.");
				return 0;
			}
		}

}
