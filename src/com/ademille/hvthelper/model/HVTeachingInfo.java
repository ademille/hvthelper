/*
 * Copyright (C) 2009  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ademille.hvthelper.model;

public class HVTeachingInfo {
	private TeachingAssignmentInfo htInfo;
	private TeachingAssignmentInfo vtInfo;
	private String version; //The version of the model.
	
	public HVTeachingInfo(TeachingAssignmentInfo htInfo, TeachingAssignmentInfo vtInfo){
		this.htInfo = htInfo;
		this.vtInfo = vtInfo;
		updateVersion();
	}
	
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	
	public void updateVersion(){
		version = "1.2.0";
	}

	public HVTeachingInfo(){
		
	}

	/**
	 * @return the htInfo
	 */
	public TeachingAssignmentInfo getHtInfo() {
		return htInfo;
	}

	/**
	 * @param htInfo the htInfo to set
	 */
	public void setHtInfo(TeachingAssignmentInfo htInfo) {
		this.htInfo = htInfo;
	}

	/**
	 * @return the vtInfo
	 */
	public TeachingAssignmentInfo getVtInfo() {
		return vtInfo;
	}

	/**
	 * @param vtInfo the vtInfo to set
	 */
	public void setVtInfo(TeachingAssignmentInfo vtInfo) {
		this.vtInfo = vtInfo;
	}
	
	

}
